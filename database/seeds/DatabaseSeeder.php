<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        User::create(['nombre' => 'Juan Mellado', 'usuario' => 'j.melladojimenez@hotmail.com', 'password' => bcrypt('juan.45753889'), 'tipo' => 1]);
        User::create(['nombre' => 'Carlos Carrillo', 'usuario' => 'carlosandresfavoritos@gmail.com', 'password' => bcrypt('favoritosandrescarlos'), 'tipo' => 1]);
    }
}
