<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inicio');
});

//Route::get('/', 'HomeController@inicio');
Route::get('/ciudad/{id}/escorts', 'CityController@obtenerCiudadEspecifico');
Route::get('/publicaciones', 'HomeController@avisosChicas');
Route::get('/ciudad/{id}/anuncios', 'HomeController@anuncios');
Route::get('/ciudad/{id}/videos', 'HomeController@videos');
Route::get('/ciudad/{id}/perfiles', 'HomeController@perfiles');

   
    Route::get('/modelo/{id}/ver/calificar', 'UserController@calificacion')->name('perfil_modelo');
Route::post('/calificar', 'UserController@puntuacion');
Route::post('/calificar/cliente', 'UserController@puntuacioncliente');
Route::get('/noticia/{id}', 'NoticiaController@noticia');
Route::get('/aviso/{id}', 'AvisoController@verAviso');
Route::get('/ciudad/{id}', 'HomeController@ciudades');
Route::get('/video/{id}', 'VideoController@verVideo');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/modelo/{id}/ver', 'UserController@verModelo')->name('perfil_modelo');

Auth::routes();

Route::get('/migrate', function() {
    Artisan::call('migrate', ['--seed' => true]);
    return 'DONE'; //Return anything
});

Route::middleware(['auth'])->group(function(){
    Route::get('/obtenerUsuario', 'UserController@obtenerUsuario');
    Route::post('/obtenerUsuario', 'UserController@obtenerUsuarioEspecifico');
    Route::post('/registrarUsuario', 'UserController@registrarUsuario');
    Route::post('/actualizarUsuario', 'UserController@actualizarUsuario');
    Route::post('/actualizarUsuarioAviso', 'UserController@actualizarUsuarioAviso');
    Route::get('/obtenerUsuarios', 'UserController@obtenerUsuarios');
 
    Route::get('/editarUsuario/{id}', 'UserController@editarUsuario');

    Route::post('/usuario/quitarPrincipal', 'UserController@quitarPrincipal');
    Route::post('/usuario/subirPrincipal', 'UserController@subirPrincipal');

    Route::post('/usuario/quitarSlider', 'UserController@quitarSlider');
    Route::post('/usuario/subirSlider', 'UserController@subirSlider');

    Route::post('/usuario/quitarAgencia', 'UserController@quitarAgencia');
    Route::post('/usuario/agregarAgencia', 'UserController@agregarAgencia');
    
    Route::post('/usuario/ocultarModelo', 'UserController@ocultarModelo');
    Route::post('/usuario/mostrarModelo', 'UserController@mostrarModelo');

    Route::post('/usuario/eliminar', 'UserController@eliminarModelo');

    Route::get('/obtenerPagina', 'PageController@obtenerPagina');
    Route::post('/actualizarPagina', 'PageController@actualizarPagina');

    //Ciudades
    Route::get('/ciudades', 'CityController@index');
    Route::post('/ciudad/registrar', 'CityController@guardarCiudad');
    Route::post('/ciudad/actualizar', 'CityController@actualizarCiudad');
    Route::post('/ciudad/eliminar', 'CityController@eliminarCiudad');
    Route::post('/ciudad/restaurar', 'CityController@restaurarCiudad');

    Route::get('/avisosUsuario', 'AvisoController@indexUsuario');
    Route::post('/aviso/registrar', 'AvisoController@guardarAviso');
    Route::post('/aviso/actualizar', 'AvisoController@actualizarAviso');
    Route::post('/aviso/eliminar', 'AvisoController@eliminarAviso');

    //Rutas de sucursales
    Route::get('/noticias', 'NoticiaController@index');
    Route::post('/noticia/registrar', 'NoticiaController@guardarNoticia');
    Route::post('/noticia/actualizar', 'NoticiaController@actualizarNoticia');
    Route::post('/noticia/eliminar', 'NoticiaController@eliminarNoticia');
    Route::post('/noticia/restaurar', 'NoticiaController@restaurarNoticia');

    //Videos
    Route::post('/video/usuario', 'VideoController@indexUser');
    Route::get('/videosUsuario', 'VideoController@indexUsuario');
    Route::post('/video/registrar', 'VideoController@guardarVideo');
    Route::post('/video/eliminar', 'VideoController@eliminarVideo');
    Route::post('/video/registrar/usuario', 'VideoController@guardarVideoUsuario');
    
    
    

    
});

