<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'usuario', 'password','busto', 'cintura', 'caderas', 'descripcion', 'tipo', 'perfil_url', 'sexo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getUrlsAttribute(){
        $fotos = array();

        if($this->foto_uno_url != null) array_push($fotos, $this->foto_uno_url);
        if($this->foto_dos_url != null) array_push($fotos, $this->foto_dos_url);
        if($this->foto_tres_url != null) array_push($fotos, $this->foto_tres_url);
        if($this->foto_cuatro_url != null) array_push($fotos, $this->foto_cuatro_url);
        if($this->foto_cinco_url != null) array_push($fotos, $this->foto_cinco_url);
        if($this->foto_seis_url != null) array_push($fotos, $this->foto_seis_url);
        if($this->foto_siete_url != null) array_push($fotos, $this->foto_siete_url);
        if($this->foto_ocho_url != null) array_push($fotos, $this->foto_ocho_url);
        if($this->foto_nueve_url != null) array_push($fotos, $this->foto_nueve_url);
        if($this->foto_diez_url != null) array_push($fotos, $this->foto_diez_url);
        if($this->foto_once_url != null) array_push($fotos, $this->foto_once_url);
        if($this->foto_doce_url != null) array_push($fotos, $this->foto_doce_url);
        
        return $fotos;
  	}
}
