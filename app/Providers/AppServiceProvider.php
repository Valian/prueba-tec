<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\User;
use App\Page;
use App\State;
use App\City;
use App\Noticia;
use App\Aviso;
use App\Valoracion;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
         //$estados = State::orderBy('created_at', 'DESC')->limit(10)->get();
        
        $ciudades = City::orderBy('nombre', 'ASC')->get();
        

         //$slider = User::where('perfil_url', '!=', '')->where('slider', 1)->where('visible', 1)->orderBy('created_At','DESC')->get();
         //$vip = User::where('perfil_url', '!=', '')->where('tipo', 2)->where('agencia', 1)->where('visible', 1)->orderBy('created_At','DESC')->get();
         //$nuevas = User::where('perfil_url', '!=', '')->where('tipo', 2)->where('visible', 1)->where('agencia', 0)->where('slider', 0)->orderBy('created_At','DESC')->get();
         //$noticias = Noticia::orderBy('created_at', 'DESC')->get();
         //$anuncios = Aviso::where('tipo', 2)->orderBy('created_at', 'DESC')->limit(25)->get();
         //$avisos = Aviso::where('tipo', 1)->orderBy('created_at', 'DESC')->limit(25)->get();
         //$index =  User::where('perfil_url', '!=', '')->where('home', 1)->where('visible', 1)->orderBy('created_At','DESC')->take(12)->get();

         View::share('ciudades', $ciudades);
         //View::share('noticias', $noticias);
         //View::share('estados', $estados);
         //View::share('vip', $vip);
         //View::share('index', $index);
         //View::share('slider', $slider);
         //View::share('nuevas', $nuevas);
         //View::share('avisos', $avisos);
         //View::share('anuncios', $anuncios);
    }
}
