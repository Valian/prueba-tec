<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;

class StateController extends Controller
{
    //
    public function obtenerEstados(Request $request){
        if (!$request->ajax()) return redirect('/home');
        
        $estados = State::where('user_id', \Auth::user()->id)->orderBy('created_at', 'DESC')->get();

        return ['estados' => $estados];
    }

    public function registrarEstado(Request $request){
        if (!$request->ajax()) return redirect('/home');
        
        $estado = new State();
        
        $estado->estado = $request->estado;
        $estado->user_id = \Auth::user()->id;
        $estado->save();
    }

    public function eliminarEstado(Request $request){
        if (!$request->ajax()) return redirect('/home');
        
        $estado = State::findOrFail($request->id);
        $estado->delete();
    }
}
