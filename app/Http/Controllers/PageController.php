<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PageController extends Controller
{
    //

    public function obtenerPagina(Request $request){
        if (!$request->ajax()) return redirect('/home');
        
        $pagina = Page::findOrFail(1);

        return ['pagina' => $pagina];
    }

    public function actualizarPagina(Request $request){
        if (!$request->ajax()) return redirect('/home');
        
        $pagina = Page::findOrFail(1);
        $pagina->email = $request->correo;
        $pagina->telefono = $request->telefono;
        $pagina->save();
    }
}
