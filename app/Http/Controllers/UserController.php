<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Video;
use App\Valoracion;
use App\Valoracion_cliente;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    public function obtenerUsuario(Request $request){
        if (!$request->ajax()) return redirect('/home');
        
        $usuario = User::findOrFail(\Auth::user()->id);

        return ['usuario' => $usuario];
    }

    public function obtenerUsuarioEspecifico(Request $request){    
        if (!$request->ajax()) return redirect('/home');
        
        $usuario = User::findOrFail($request->id);

        return ['usuario' => $usuario];
    }

    public function obtenerUsuarios(Request $request){
        if (!$request->ajax() || \Auth()->user()->tipo != 1) return redirect('/home');
        
        $usuarios = User::where('tipo', 2)->orderBy('nombre', 'desc')->get();

        return ['usuarios' => $usuarios];
    }

    public function registrarUsuario(Request $request){
        if (!$request->ajax()) return redirect('/home');
        
        $usuario = new User();

        $usuario->nombre = $request->alias;
        $usuario->telefono = $request->telefono;
        $usuario->usuario = $request->usuario;
        $usuario->password = Hash::make($request->clave);
        $usuario->altura = $request->altura;
        $usuario->busto = $request->busto;
        $usuario->cintura = $request->cintura;
        $usuario->caderas = $request->caderas;
        $usuario->descripcion = $request->descripcion;
        $usuario->edad = $request->edad;
        $usuario->ciudad_id = $request->ciudad_id;
        $usuario->nacionalidad = $request->nacionalidad;

        $usuario->save();

        $usuarioCreado = User::where('nombre', $usuario->nombre)->where('created_at', $usuario->created_at)->first();

        if ($request->hasFile('video')) {

            if($usuarioCreado->video != ''){
                Storage::delete($usuarioCreado->video);
            }

            $video = $request->file('video');
            $extension = $video->getClientOriginalExtension();

            Storage::disk('public')->put('videos/' . $usuarioCreado->id . '-1.' . $extension,  \File::get($video));

            $usuarioCreado->video_url = 'videos/' . $usuarioCreado->id . '-1.' . $extension;
        } 

        if ($request->hasFile('foto_perfil')) {

            if($usuarioCreado->perfil_url != ''){
                Storage::delete($usuarioCreado->perfil_url);
            }

            $foto_perfil = $request->file('foto_perfil');
            $extension = $foto_perfil->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_perfil/' . $usuarioCreado->id . '-1.' . $extension,  \File::get($foto_perfil));

            $usuarioCreado->perfil_url = 'fotos_perfil/' . $usuarioCreado->id . '-1.' . $extension;
        } 

        if ($request->hasFile('foto_uno')) {

            if($usuarioCreado->foto_uno_url != ''){
                Storage::delete($usuarioCreado->foto_uno_url);
            }

            $foto_uno = $request->file('foto_uno');
            $extension = $foto_uno->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuarioCreado->id . '-1.' . $extension,  \File::get($foto_uno));

            $usuarioCreado->foto_uno_url = 'fotos_galeria/' . $usuarioCreado->id . '-1.' . $extension;
        } 

        if ($request->hasFile('foto_dos')) {

            if($usuarioCreado->foto_dos_url != ''){
                Storage::delete($usuarioCreado->foto_dos_url);
            }

            $foto_dos = $request->file('foto_dos');
            $extension = $foto_dos->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuarioCreado->id . '-2.' . $extension,  \File::get($foto_dos));

            $usuarioCreado->foto_dos_url = 'fotos_galeria/' . $usuarioCreado->id . '-2.' . $extension;
        }
        
        if ($request->hasFile('foto_tres')) {

            if($usuarioCreado->foto_tres_url != ''){
                Storage::delete($usuarioCreado->foto_tres_url);
            }

            $foto_tres = $request->file('foto_tres');
            $extension = $foto_tres->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuarioCreado->id . '-3.' . $extension,  \File::get($foto_tres));

            $usuarioCreado->foto_tres_url = 'fotos_galeria/' . $usuarioCreado->id . '-3.' . $extension;
        }

        if ($request->hasFile('foto_cuatro')) {

            if($usuarioCreado->foto_cuatro_url != ''){
                Storage::delete($usuarioCreado->foto_cuatro_url);
            }

            $foto_cuatro = $request->file('foto_cuatro');
            $extension = $foto_cuatro->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuarioCreado->id . '-4.' . $extension,  \File::get($foto_cuatro));

            $usuarioCreado->foto_cuatro_url = 'fotos_galeria/' . $usuarioCreado->id . '-4.' . $extension;
        }

        if ($request->hasFile('foto_cinco')) {

            if($usuarioCreado->foto_cinco_url != ''){
                Storage::delete($usuarioCreado->foto_cinco_url);
            }

            $foto_cinco = $request->file('foto_cinco');
            $extension = $foto_cinco->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuarioCreado->id . '-5.' . $extension,  \File::get($foto_cinco));

            $usuarioCreado->foto_cinco_url = 'fotos_galeria/' . $usuarioCreado->id . '-5.' . $extension;
        }

        if ($request->hasFile('foto_seis')) {

            if($usuarioCreado->foto_seis_url != ''){
                Storage::delete($usuarioCreado->foto_seis_url);
            }

            $foto_seis = $request->file('foto_seis');
            $extension = $foto_seis->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuarioCreado->id . '-6.' . $extension,  \File::get($foto_seis));

            $usuarioCreado->foto_seis_url = 'fotos_galeria/' . $usuarioCreado->id . '-6.' . $extension;
        }

        if ($request->hasFile('foto_siete')) {

            if($usuarioCreado->foto_siete_url != ''){
                Storage::delete($usuarioCreado->foto_siete_url);
            }

            $foto_siete = $request->file('foto_siete');
            $extension = $foto_siete->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuarioCreado->id . '-7.' . $extension,  \File::get($foto_siete));

            $usuarioCreado->foto_siete_url = 'fotos_galeria/' . $usuarioCreado->id . '-7.' . $extension;
        }

        if ($request->hasFile('foto_ocho')) {

            if($usuarioCreado->foto_ocho_url != ''){
                Storage::delete($usuarioCreado->foto_ocho_url);
            }

            $foto_ocho = $request->file('foto_ocho');
            $extension = $foto_ocho->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuarioCreado->id . '-8.' . $extension,  \File::get($foto_ocho));

            $usuarioCreado->foto_ocho_url = 'fotos_galeria/' . $usuarioCreado->id . '-8.' . $extension;
        }

        if ($request->hasFile('foto_nueve')) {

            if($usuarioCreado->foto_nueve_url != ''){
                Storage::delete($usuarioCreado->foto_nueve_url);
            }

            $foto_nueve = $request->file('foto_nueve');
            $extension = $foto_nueve->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuarioCreado->id . '-9.' . $extension,  \File::get($foto_nueve));

            $usuarioCreado->foto_nueve_url = 'fotos_galeria/' . $usuarioCreado->id . '-9.' . $extension;
        }

        if ($request->hasFile('foto_diez')) {

            if($usuarioCreado->foto_diez_url != ''){
                Storage::delete($usuarioCreado->foto_diez_url);
            }

            $foto_diez = $request->file('foto_diez');
            $extension = $foto_diez->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuarioCreado->id . '-10.' . $extension,  \File::get($foto_diez));

            $usuarioCreado->foto_diez_url = 'fotos_galeria/' . $usuarioCreado->id . '-10.' . $extension;
        }

        if ($request->hasFile('foto_once')) {

            if($usuarioCreado->foto_once_url != ''){
                Storage::delete($usuarioCreado->foto_once_url);
            }

            $foto_once = $request->file('foto_once');
            $extension = $foto_once->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuarioCreado->id . '-11.' . $extension,  \File::get($foto_once));

            $usuarioCreado->foto_once_url = 'fotos_galeria/' . $usuarioCreado->id . '-11.' . $extension;
        }

        if ($request->hasFile('foto_doce')) {

            if($usuarioCreado->foto_doce_url != ''){
                Storage::delete($usuarioCreado->foto_doce_url);
            }

            $foto_doce = $request->file('foto_doce');
            $extension = $foto_doce->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuarioCreado->id . '-12.' . $extension,  \File::get($foto_doce));

            $usuarioCreado->foto_doce_url = 'fotos_galeria/' . $usuarioCreado->id . '-12.' . $extension;
        }

        $usuarioCreado->save();
    }

    public function actualizarUsuario(Request $request){
        if (!$request->ajax()) return redirect('/home');

        $usuario = User::findOrFail(\Auth::user()->id);

        if($request->id_modelo > 0){
            $usuario = User::findOrFail($request->id_modelo);
        }
        

        if($request->clave != null){
            $usuario->password = Hash::make($request->clave);
        }
        
        $usuario->nombre = $request->nombre;
        $usuario->telefono = $request->telefono;
        $usuario->altura = $request->altura;
        $usuario->busto = $request->busto;
        $usuario->cintura = $request->cintura;
        $usuario->caderas = $request->caderas;
        $usuario->descripcion = $request->descripcion;
        $usuario->edad = $request->edad;
        $usuario->ciudad_id = $request->ciudad_id;
        $usuario->nacionalidad = $request->nacionalidad;

        if ($request->hasFile('video')) {
            if($usuario->video_url != ''){
                Storage::delete($usuario->video_url);
            }

            $video_url = $request->file('video');
            $extension = $video_url->getClientOriginalExtension();

            Storage::disk('public')->put('videos/' . $usuario->id . '-1.' . $extension,  \File::get($video_url));

            $usuario->video_url = 'videos/' . $usuario->id . '-1.' . $extension;
        } 

        if ($request->hasFile('foto_perfil')) {

            if($usuario->perfil_url != ''){
                Storage::delete($usuario->perfil_url);
            }

            $foto_perfil = $request->file('foto_perfil');
            $extension = $foto_perfil->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_perfil/' . $usuario->id . '-1.' . $extension,  \File::get($foto_perfil));

            $usuario->perfil_url = 'fotos_perfil/' . $usuario->id . '-1.' . $extension;
        } 

        if ($request->hasFile('foto_uno')) {

            if($usuario->foto_uno_url != ''){
                Storage::delete($usuario->foto_uno_url);
            }

            $foto_uno = $request->file('foto_uno');
            $extension = $foto_uno->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-1.' . $extension,  \File::get($foto_uno));

            $usuario->foto_uno_url = 'fotos_galeria/' . $usuario->id . '-1.' . $extension;
        } 

        if ($request->hasFile('foto_dos')) {

            if($usuario->foto_dos_url != ''){
                Storage::delete($usuario->foto_dos_url);
            }

            $foto_dos = $request->file('foto_dos');
            $extension = $foto_dos->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-2.' . $extension,  \File::get($foto_dos));

            $usuario->foto_dos_url = 'fotos_galeria/' . $usuario->id . '-2.' . $extension;
        }
        
        if ($request->hasFile('foto_tres')) {

            if($usuario->foto_tres_url != ''){
                Storage::delete($usuario->foto_tres_url);
            }

            $foto_tres = $request->file('foto_tres');
            $extension = $foto_tres->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-3.' . $extension,  \File::get($foto_tres));

            $usuario->foto_tres_url = 'fotos_galeria/' . $usuario->id . '-3.' . $extension;
        }

        if ($request->hasFile('foto_cuatro')) {

            if($usuario->foto_cuatro_url != ''){
                Storage::delete($usuario->foto_cuatro_url);
            }

            $foto_cuatro = $request->file('foto_cuatro');
            $extension = $foto_cuatro->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-4.' . $extension,  \File::get($foto_cuatro));

            $usuario->foto_cuatro_url = 'fotos_galeria/' . $usuario->id . '-4.' . $extension;
        }

        if ($request->hasFile('foto_cinco')) {

            if($usuario->foto_cinco_url != ''){
                Storage::delete($usuario->foto_cinco_url);
            }

            $foto_cinco = $request->file('foto_cinco');
            $extension = $foto_cinco->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-5.' . $extension,  \File::get($foto_cinco));

            $usuario->foto_cinco_url = 'fotos_galeria/' . $usuario->id . '-5.' . $extension;
        }

        if ($request->hasFile('foto_seis')) {

            if($usuario->foto_seis_url != ''){
                Storage::delete($usuario->foto_seis_url);
            }

            $foto_seis = $request->file('foto_seis');
            $extension = $foto_seis->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-6.' . $extension,  \File::get($foto_seis));

            $usuario->foto_seis_url = 'fotos_galeria/' . $usuario->id . '-6.' . $extension;
        }

        if ($request->hasFile('foto_siete')) {

            if($usuario->foto_siete_url != ''){
                Storage::delete($usuario->foto_siete_url);
            }

            $foto_siete = $request->file('foto_siete');
            $extension = $foto_siete->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-7.' . $extension,  \File::get($foto_siete));

            $usuario->foto_siete_url = 'fotos_galeria/' . $usuario->id . '-7.' . $extension;
        }

        if ($request->hasFile('foto_ocho')) {

            if($usuario->foto_ocho_url != ''){
                Storage::delete($usuario->foto_ocho_url);
            }

            $foto_ocho = $request->file('foto_ocho');
            $extension = $foto_ocho->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-8.' . $extension,  \File::get($foto_ocho));

            $usuario->foto_ocho_url = 'fotos_galeria/' . $usuario->id . '-8.' . $extension;
        }

        if ($request->hasFile('foto_nueve')) {

            if($usuario->foto_nueve_url != ''){
                Storage::delete($usuario->foto_nueve_url);
            }

            $foto_nueve = $request->file('foto_nueve');
            $extension = $foto_nueve->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-9.' . $extension,  \File::get($foto_nueve));

            $usuario->foto_nueve_url = 'fotos_galeria/' . $usuario->id . '-9.' . $extension;
        }

        if ($request->hasFile('foto_diez')) {

            if($usuario->foto_diez_url != ''){
                Storage::delete($usuario->foto_diez_url);
            }

            $foto_diez = $request->file('foto_diez');
            $extension = $foto_diez->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-10.' . $extension,  \File::get($foto_diez));

            $usuario->foto_diez_url = 'fotos_galeria/' . $usuario->id . '-10.' . $extension;
        }

        if ($request->hasFile('foto_once')) {

            if($usuario->foto_once_url != ''){
                Storage::delete($usuario->foto_once_url);
            }

            $foto_once = $request->file('foto_once');
            $extension = $foto_once->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-11.' . $extension,  \File::get($foto_once));

            $usuario->foto_once_url = 'fotos_galeria/' . $usuario->id . '-11.' . $extension;
        }

        if ($request->hasFile('foto_doce')) {

            if($usuario->foto_doce_url != ''){
                Storage::delete($usuario->foto_doce_url);
            }

            $foto_doce = $request->file('foto_doce');
            $extension = $foto_doce->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-12.' . $extension,  \File::get($foto_doce));

            $usuario->foto_doce_url = 'fotos_galeria/' . $usuario->id . '-12.' . $extension;
        }

        $usuario->save();
    }

    public function actualizarUsuarioAviso(Request $request){
        if (!$request->ajax()) return redirect('/home');

        $usuario = User::findOrFail(\Auth::user()->id);
        
        $usuario->nombre = $request->nombre;
        $usuario->telefono = $request->telefono;
        $usuario->descripcion = $request->descripcion;
        $usuario->edad = $request->edad;
        $usuario->ciudad_id = $request->ciudad_id;
        $usuario->nacionalidad = $request->nacionalidad;

        if ($request->hasFile('foto_perfil')) {

            if($usuario->perfil_url != ''){
                Storage::delete($usuario->perfil_url);
            }

            $foto_perfil = $request->file('foto_perfil');
            $extension = $foto_perfil->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_perfil/' . $usuario->id . '-1.' . $extension,  \File::get($foto_perfil));

            $usuario->perfil_url = 'fotos_perfil/' . $usuario->id . '-1.' . $extension;
        } 

        if ($request->hasFile('foto_uno')) {

            if($usuario->foto_uno_url != ''){
                Storage::delete($usuario->foto_uno_url);
            }

            $foto_uno = $request->file('foto_uno');
            $extension = $foto_uno->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-1.' . $extension,  \File::get($foto_uno));

            $usuario->foto_uno_url = 'fotos_galeria/' . $usuario->id . '-1.' . $extension;
        } 

        if ($request->hasFile('foto_dos')) {

            if($usuario->foto_dos_url != ''){
                Storage::delete($usuario->foto_dos_url);
            }

            $foto_dos = $request->file('foto_dos');
            $extension = $foto_dos->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-2.' . $extension,  \File::get($foto_dos));

            $usuario->foto_dos_url = 'fotos_galeria/' . $usuario->id . '-2.' . $extension;
        }
        
        if ($request->hasFile('foto_tres')) {

            if($usuario->foto_tres_url != ''){
                Storage::delete($usuario->foto_tres_url);
            }

            $foto_tres = $request->file('foto_tres');
            $extension = $foto_tres->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-3.' . $extension,  \File::get($foto_tres));

            $usuario->foto_tres_url = 'fotos_galeria/' . $usuario->id . '-3.' . $extension;
        }

        if ($request->hasFile('foto_cuatro')) {

            if($usuario->foto_cuatro_url != ''){
                Storage::delete($usuario->foto_cuatro_url);
            }

            $foto_cuatro = $request->file('foto_cuatro');
            $extension = $foto_cuatro->getClientOriginalExtension();

            Storage::disk('public')->put('fotos_galeria/' . $usuario->id . '-4.' . $extension,  \File::get($foto_cuatro));

            $usuario->foto_cuatro_url = 'fotos_galeria/' . $usuario->id . '-4.' . $extension;
        }
        
        $usuario->save();
    }

    public function verModelo($id){
        $usuario = User::findOrFail($id);
        $videos = Video::where('user_id', $id)->orderBy('titulo')->get();
        return view('perfil')->with(compact('usuario', 'videos'));
    }

    public function editarUsuario($id){
        $usuario_id = $id;
        return view('admin.usuario')->with(compact('usuario_id'));
    }

    public function quitarPrincipal(Request $request){
        if (!$request->ajax() || \Auth()->user()->tipo != 1) return redirect('/home');
        $usuario = User::findOrFail($request->id);
        $usuario->home = 0;
        $usuario->save();
    }

    public function subirPrincipal(Request $request){
        if (!$request->ajax() || \Auth()->user()->tipo != 1) return redirect('/home');
        $usuario = User::findOrFail($request->id);
        $usuario->home = 1;
        $usuario->save();
    }

    public function quitarSlider(Request $request){
        if (!$request->ajax() || \Auth()->user()->tipo != 1) return redirect('/home');
        $usuario = User::findOrFail($request->id);
        $usuario->slider = 0;
        $usuario->save();
    }

    public function subirSlider(Request $request){
        if (!$request->ajax() || \Auth()->user()->tipo != 1) return redirect('/home');
        $usuario = User::findOrFail($request->id);
        $usuario->slider = 1;
        $usuario->save();
    }

    public function ocultarModelo(Request $request){
        if (!$request->ajax() || \Auth()->user()->tipo != 1) return redirect('/home');
        $usuario = User::findOrFail($request->id);
        $usuario->visible = 0;
        $usuario->save();
    }

    public function mostrarModelo(Request $request){
        if (!$request->ajax() || \Auth()->user()->tipo != 1) return redirect('/home');
        $usuario = User::findOrFail($request->id);
        $usuario->visible = 1;
        $usuario->save();
    }

    public function quitarAgencia(Request $request){
        if (!$request->ajax() || \Auth()->user()->tipo != 1) return redirect('/home');
        $usuario = User::findOrFail($request->id);
        $usuario->agencia = 0;
        $usuario->save();
    }

    public function agregarAgencia(Request $request){
        if (!$request->ajax() || \Auth()->user()->tipo != 1) return redirect('/home');
        $usuario = User::findOrFail($request->id);
        $usuario->agencia = 1;
        $usuario->save();
    }

    public function eliminarModelo(Request $request){
        if (!$request->ajax() || \Auth()->user()->tipo != 1) return redirect('/home');
        $usuario = User::findOrFail($request->id);

        if($usuario->perfil_url != ''){
            Storage::delete($usuario->perfil_url);
        }

        if($usuario->foto_uno_url != ''){
            Storage::delete($usuario->foto_uno_url);
        }
        
        if($usuario->foto_dos_url != ''){
            Storage::delete($usuario->foto_dos_url);
        }
        
        if($usuario->foto_tres_url != ''){
            Storage::delete($usuario->foto_tres_url);
        }
        
        if($usuario->foto_cuatro_url != ''){
            Storage::delete($usuario->foto_cuatro_url);
        }

        $usuario->delete();
    }
     /*public function calificacion(Request $request){
         $calificar = new calificar();
          $calificar->id_usuario = $request->id_usuario;
        $calificar->atencion = $request->atencion;
        $calificar->higiene = $request->higiene;
        $calificar->fotos = $request->fotos;
        $calificar->nota_final = $request->nota_final;


        $calificar->save();
        
    }*/
    
    public function calificacion() {
  
  return view('calificar');
  

}
 public function puntuacion(Request $request) {
     
     /*dd($request->all());*/
     $calificar = new Valoracion;
     $calificar->id_usuario=$request->input('id_usuario');
 $calificar->atencion=$request->input('atencion');
 $calificar->higiene=$request->input('higiene');
  $calificar->fotos=$request->input('fotos');
   $calificar->nota_final=$request->input('nota_final');
 $calificar->save();
 return back()->with('message','Success');
}

 public function puntuacioncliente(Request $request) {
     
     /*dd($request->all());*/
     $calificar_cliente = new Valoracion_cliente;
     $calificar_cliente->nickname=$request->input('nickname');
 $calificar_cliente->higiene=$request->input('higiene');
 $calificar_cliente->respeto=$request->input('respeto');
  $calificar_cliente->dinero=$request->input('dinero');
   $calificar_cliente->nota_final=$request->input('nota_final');
   $calificar_cliente->id_modelo=$request->input('id_modelo');
 $calificar_cliente->save();
 return back()->with('message','Success');
}


}
