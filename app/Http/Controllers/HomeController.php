<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;
use App\User;
use App\Aviso;
use App\Video;
use App\Valoracion;
use App\Valoracion_cliente;
use SEO;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        if(\Auth::user()->tipo == 1){
            return view('admin.contenido');
        } else {
            return view('user.contenido');
        }
        
    }

    public function inicio(){
        return view('index');
    }

    public function avisosChicas(){
        $avisos = Aviso::where('tipo', 1)->orderBy('created_at', 'DESC')->limit(25)->get();
        return view('avisos')->with(compact('avisos'));
    }

    public function anuncios(){
        $anuncios = Aviso::where('tipo', 1)->orderBy('created_at', 'DESC')->limit(25)->get();
        return view('anuncios')->with(compact('anuncios'));
    }

    public function videos($id){
        $videos = Video::distinct()->select('user_id')->groupBy('user_id')->get();
        $usuarios = User::distinct()->select('id')->where('video_url', '!=', NULL)->get();  

        $perfil_videos = $videos->merge($usuarios);
        $perfil_usuarios = $perfil_videos->unique();

        return view('videos')->with(compact('perfil_usuarios'));
    }
    
     public function perfiles($id){

        $usuarios = User::distinct()->select('id')->where('video_url', '!=', NULL)->get();

        $perfiles = User::where('perfil_url', '!=', '')->where('video_url', '!=', NULL)->where('tipo', 2)->where('visible', 1)->where('slider', 0)->orderBy('created_At','DESC')->get();


     $perfil_perfiles = $perfiles->merge($usuarios);
        $perfil_usuarios_2 = $perfil_perfiles->unique();

        return view('perfiles')->with(compact('perfil_usuarios_2'));
    }

    public function home(){

        $estados = State::orderBy('created_at', 'DESC')->limit(10)->get();
        $slider = User::where('perfil_url', '!=', '')->where('ciudad_id', 1)->where('slider', 1)->where('visible', 1)->orderBy('created_At','DESC')->get();
        $vip = User::where('perfil_url', '!=', '')->where('ciudad_id', 1)->where('tipo', 2)->where('agencia', 1)->where('visible', 1)->orderBy('created_At','DESC')->get();
        $nuevas = User::where('perfil_url', '!=', '')->where('ciudad_id', 1)->where('tipo', 2)->where('visible', 1)->where('agencia', 0)->where('slider', 0)->orderBy('created_At','DESC')->get();
        return view('welcome')->with(compact('estados', 'slider', 'vip', 'nuevas'));
    }

    public function ciudades($id){

         //$slider = User::where('perfil_url', '!=', '')->where('slider', 1)->where('visible', 1)->orderBy('created_At','DESC')->get();
         //$vip = User::where('perfil_url', '!=', '')->where('tipo', 2)->where('agencia', 1)->where('visible', 1)->orderBy('created_At','DESC')->get();
         //$nuevas = User::where('perfil_url', '!=', '')->where('tipo', 2)->where('visible', 1)->where('agencia', 0)->where('slider', 0)->orderBy('created_At','DESC')->get();
         //$noticias = Noticia::orderBy('created_at', 'DESC')->get();
         //$anuncios = Aviso::where('tipo', 2)->orderBy('created_at', 'DESC')->limit(25)->get();
        $avisos = Aviso::where('tipo', 1)->orderBy('created_at', 'DESC')->with('usuario')->limit(25)->get();
         //$index =  User::where('perfil_url', '!=', '')->where('home', 1)->where('visible', 1)->orderBy('created_At','DESC')->take(12)->get();
         $valoracion = Valoracion::where('nota_final','>=','5')->groupBy('id_usuario')->orderBy('id', 'DESC')->get();
        
        //$estados = State::orderBy('created_at', 'DESC')->limit(10)->get();
        //$slider = User::where('perfil_url', '!=', '')->where('ciudad_id', $id)->where('slider', 1)->where('visible', 1)->orderBy('created_At','DESC')->get();
        //$vip = User::where('perfil_url', '!=', '')->where('ciudad_id', $id)->where('tipo', 2)->where('agencia', 1)->where('visible', 1)->orderBy('created_At','DESC')->get();
        $escorts = User::where('perfil_url', '!=', '')->where('ciudad_id', $id)->where('tipo', 2)->where('visible', 1)->where('agencia', 1)->where('slider', 0)->orderBy('created_At','DESC')->get();
         $todas = User::where('perfil_url', '!=', '')->where('ciudad_id', $id)->where('tipo', 2)->where('visible', 1)->where('slider', 0)->orderBy('created_At','DESC')->get();
        $avisos = $avisos->reject(function ($aviso)  use ($id)  { return $aviso->usuario->ciudad_id != $id; });
        $ciudad = $id;
         
        return view('welcome')->with(compact('avisos', 'escorts', 'ciudad', 'todas', 'valoracion'));
    }
    
   
}
