<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class CityController extends Controller
{
    
    
      public function obtenerCiudadEspecifico(Request $request){    
        if (!$request->ajax()) return redirect('/ciudad/1');
        
        $ciudad = City::findOrFail($request->id);

        return ['ciudad' => $ciudad];
    }
    //
    public function index(Request $request){
        if (!$request->ajax()) return redirect('/');
        
        $ciudades = City::orderBy('nombre', 'asc')->withTrashed()->get();

        return [
            'ciudades' => $ciudades
        ];
    }

    public function guardarCiudad(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $ciudad = new City();
        $ciudad->nombre = $request->nombre;
        $ciudad->save();
    }

    public function actualizarCiudad(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $ciudad = City::findOrFail($request->ciudad_id);
        $ciudad->nombre = $request->nombre;
        $ciudad->save();
    }

    public function eliminarCiudad(Request $request){
        if (!$request->ajax()) return redirect('/');
        
        City::findOrFail($request->id)->delete();
    }

    public function restaurarCiudad(Request $request){
        if (!$request->ajax()) return redirect('/');
        
        City::withTrashed()->find($request->id)->restore();
    }
}
