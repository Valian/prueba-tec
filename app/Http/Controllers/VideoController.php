<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\User;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    //
    public function indexUsuario(Request $request){
        if (!$request->ajax()) return redirect('/');
        
        $videos = Video::where('user_id', \Auth::user()->id)->orderBy('titulo', 'asc')->get();

        return [
            'videos' => $videos
        ];
    }

    public function indexUser(Request $request){
        if (!$request->ajax()) return redirect('/');
        
        $videos = Video::where('user_id', $request->id)->orderBy('titulo', 'asc')->get();

        return [
            'videos' => $videos
        ];
    }

    public function guardarVideo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $video = new Video();
        $video->titulo = $request->titulo;

        if ($request->hasFile('video')) {

            $video->video_url = Storage::disk('public')->putFile('videos_usuarios', $request->file('video'));
        } 

        $video->user_id = \Auth::user()->id;
        $video->save();
    }

    public function guardarVideoUsuario(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $video = new Video();
        $video->titulo = $request->titulo;

        if ($request->hasFile('video')) {

            $video->video_url = Storage::disk('public')->putFile('videos_usuarios', $request->file('video'));
        } 

        $video->user_id = $request->usuario_id;
        $video->save();
    }


    public function eliminarVideo(Request $request){
        if (!$request->ajax()) return redirect('/');
        
        $video = Video::findOrFail($request->id);
        
        Storage::delete($video->video_url);

        $video->delete();
    }

    public function verVideo(Request $request){      
        $usuario = User::findOrfail($request->id);
        $videos = Video::where('user_id', $request->id)->orderBy('titulo')->get();
        return view('video')->with(compact('videos', 'usuario'));
    }
}
