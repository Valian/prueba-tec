<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Noticia;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class NoticiaController extends Controller
{
    //
    public function index(Request $request){
        if (!$request->ajax()) return redirect('/');
        
        $noticias = Noticia::orderBy('titulo', 'asc')->get();

        return [
            'noticias' => $noticias
        ];
    }

    public function guardarNoticia(Request $request)
    {   
        $hoy = Carbon::now();
        if (!$request->ajax()) return redirect('/');

        $noticia = new Noticia();
        $noticia->titulo = $request->titulo;
        $noticia->contenido = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br />", $request->contenido);

        if ($request->hasFile('imagen')) {
            $imagen = $request->file('imagen');
            $extension = $imagen->getClientOriginalExtension();

            $url = Storage::disk('public')->put('noticias', $imagen);

            $noticia->img_url = $url;
        }
        

        $noticia->save();
    }

    public function actualizarNoticia(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $noticia = Noticia::findOrFail($request->noticia_id);
        $noticia->titulo = $request->titulo;
        $noticia->contenido = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br />", $request->contenido);
        $noticia->save();
    }

    public function eliminarNoticia(Request $request){
        if (!$request->ajax()) return redirect('/');
        
        Noticia::findOrFail($request->id)->delete();
    }

    public function noticia($id){
        $noticia = Noticia::findOrFail($id);

        return view('noticia')->with(compact('noticia'));
    }
}
