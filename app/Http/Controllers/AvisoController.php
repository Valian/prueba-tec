<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aviso;

class AvisoController extends Controller
{
    //
    public function index(Request $request){
        if (!$request->ajax()) return redirect('/');
        
        $avisos = Aviso::orderBy('titulo', 'asc')->get();

        return [
            'avisos' => $avisos
        ];
    }

    public function indexUsuario(Request $request){
        if (!$request->ajax()) return redirect('/');
        
        $avisos = Aviso::where('user_id', \Auth::user()->id)->orderBy('titulo', 'asc')->get();

        return [
            'avisos' => $avisos
        ];
    }

    public function verAviso(Request $request){      
        $aviso = Aviso::findOrFail($request->id);
        return view('aviso')->with(compact('aviso'));
    }


    public function guardarAviso(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $aviso = new Aviso();
        $aviso->titulo = $request->titulo;
        $aviso->contenido = $request->contenido;
        $aviso->user_id = \Auth::user()->id;
        $aviso->tipo = (\Auth::user()->tipo - 1);
        $aviso->save();
    }

    public function actualizarAviso(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $aviso = Aviso::findOrFail($request->aviso_id);
        $aviso->titulo = $request->titulo;
        $aviso->contenido = $request->contenido;
        $aviso->save();
    }

    public function eliminarAviso(Request $request){
        if (!$request->ajax()) return redirect('/home');
        
        $aviso = Aviso::findOrFail($request->id);
        $aviso->delete();
    }
}
