const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');
   
mix.styles([
   'public/principal/css/bootstrap.min.css',
   'public/principal/css/animations.css',
   'public/principal/css/font-awesome.css',
   'public/principal/css/main.css',
], 'public/css/principal.css')
.scripts([
   'public/principal/js/vendor/modernizr-custom.js.js',
   'public/principal/js/compressed.js',
   'public/principal/js/script.js',
   'public/principal/js/main.js'
], 'public/js/principal.js');

mix.styles([
   'public/intranet/css/pages/login-register-lock.css',
   'public/intranet/css/style.min.css'
], 'public/intranet/css/css-login.css')
.scripts([
   'public/intranet/node_modules/popper/popper.min.js',
   'public/intranet/js/perfect-scrollbar.jquery.min.js',
   'public/intranet/js/waves.js',
   'public/intranet/js/sidebarmenu.js',
   'public/intranet/js/custom.min.js',
   'public/intranet/node_modules/jquery-sparkline/jquery.sparkline.min.js',
   'public/intranet/node_modules/raphael/raphael-min.js',
   'public/intranet/node_modules/morrisjs/morris.min.js',
   'public/intranet/js/dashboard1.js',
   'public/intranet/js/sweetalert2.all.js'
], 'public/intranet/js/js-admin.js');

mix.styles([
   'public/intranet/css/pages/login-register-lock.css',
   'public/intranet/node_modules/morrisjs/morris.css',
   'public/intranet/css/style.min.css',
   'public/intranet/css/pages/dashboard1.css',
   'public/intranet/css/pages/floating-label.css'
], 'public/intranet/css/css-admin.css')
.scripts([
   'public/intranet/node_modules/popper/popper.min.js',
   'public/intranet/js/perfect-scrollbar.jquery.min.js',
   'public/intranet/js/waves.js',
   'public/intranet/js/sidebarmenu.js',
   'public/intranet/js/custom.min.js',
   'public/intranet/node_modules/jquery-sparkline/jquery.sparkline.min.js',
   'public/intranet/node_modules/raphael/raphael-min.js',
   'public/intranet/node_modules/morrisjs/morris.min.js',
   'public/intranet/js/dashboard1.js',
   'public/intranet/js/sweetalert2.all.js'
], 'public/intranet/js/js-admin.js');


