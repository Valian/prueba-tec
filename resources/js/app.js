
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('perfil-usuario', require('./components/Perfil-usuario.vue').default);
Vue.component('usuarios', require('./components/Usuarios.vue').default);
Vue.component('pagina', require('./components/Pagina.vue').default);
Vue.component('ciudad', require('./components/Ciudad.vue').default);
Vue.component('estado', require('./components/Estado.vue').default);
Vue.component('aviso', require('./components/Aviso.vue').default);
Vue.component('perfil-aviso', require('./components/Perfil-aviso.vue').default);
Vue.component('blog', require('./components/Blog.vue').default);
Vue.component('video-usuario', require('./components/Video.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data :{
        menu : 0
    }
});
