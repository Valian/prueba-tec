@extends('home')

@section('content')
    <template v-if="menu==0">
        @if(Auth::user()->tipo == 2)
            <perfil-usuario></perfil-usuario>
        @else 
            <perfil-aviso></perfil-aviso>
        @endif
    </template>

    @if(Auth::user()->tipo == 2)
        <template v-if="menu==1">
            <estado></estado>
        </template>

        <template v-if="menu==3">
            <video-usuario></video-usuario>
        </template>
    @endif

    <template v-if="menu==2">
        <aviso></aviso>
    </template>
@endsection