@extends('layouts.principal')

@section('contenido')

    <section id="publicateEscort" class="contacts-section top_mask_add ds overflow-visible background-contact s-pt-100 s-pb-60 s-pt-md-95 s-pb-md-80 s-pt-xl-170 s-pb-xl-140 c-gutter-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-xl-4 animate" data-animation="fadeInUp" data-delay="150">
                    <h2 class="mt-0 mb-40 contact-title text-uppercase">PUBLICATE EN</h2>
                    <span class="color-main fs-24 font-main text-uppercase">TUSENCUENTROS.CL</span>
                    
                    <div class="media mb-20">
                        <h5 class="fs-20 mb-0 min-w-100">Email:</h5>
                        <div class="media-body ml-0 d-flex flex-column">
                            <span>contacto@tusencuentros.cl</span>
                        </div>
                    </div>
                </div>
                <!--.col-* -->
                <div class="fw-divider-space hidden-above-lg mt-20"></div>
                <div class="col-lg-7 col-xl-8 animate" data-animation="fadeInUp" data-delay="150" style="margin: auto;">                                
                    <form class="contact-form c-mb-20 c-gutter-20" method="POST" action="{{ route('register') }}">
                        @csrf
                        <input type="hidden" id="tipo_usuario" name="tipo_usuario" value="2">
    
                        @if($errors->any())
                            <div class="row">
                                <div class="col-12">
                                    <div class="alert alert-danger text-left">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group ">
                                    <input type="text" name="usuario" class="form-control" placeholder="Usuario" style="height: 40px;border-radius: 40px;">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="password" name="clave" class="form-control" placeholder="Contraseña" style="height: 40px;border-radius: 40px;">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select name="sexo" id="sexo" class="form-control" style="height: 40px;border-radius: 40px;width: calc(100% - 40px);">
                                        <option value="1" selected>MUJER</option>
                                        <option value="2">HOMBRE</option>
                                    </select>
                                    <button type="submit" class="btn-submit" style="border-radius: 0px 20px 20px 0px;height: 40px;"><i class="fa fa-paper-plane"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--.col-* -->
            </div>
        </div>
    </section>
    
      @if(count($avisos) > 0)
    <section id="avisos" class="ds s-pt-70 s-pb-20 s-pb-sm-50 s-py-lg-100 s-py-xl-150">
        <br><br>
      <h5 class="big-title" style="font-size: 50px;line-height: initial;text-align:center">AVISOS ER&OacuteTICOS</h5>
   
        <div class="container">

            @foreach ($avisos as $item)
                <div class="row c-gutter-60" style="margin-bottom:20px;box-shadow: 2px 3px 6px -1px rgba(0,0,0,0.99);" id="avisos_c-gutter">
                    <div class="offset-lg-1 col-lg-10">
                        <article>
                            <div class="row">
                                <div class="col-xl-3 col-4" id="col_aviso_1">
                                    <div class="item-media">
                                        <center>
                                            <img src="{{ asset('storage/'. $item->usuario->perfil_url) }}" alt="img" class="img-responsive imagenes-avisos" height="200px">
                                            <div class="media-links">
                                                <a class="abs-link" title="" href="{{ url('aviso/' . $item->id) }}"></a>
                                            </div>
                                        </center>
                                    </div>
                                </div>

                                <div class="col-xl-9 col-8" id="col_aviso_2">
                                    <div class="item-content">
                                        <div class="entry-content">
                                            <h3 class="entry-title" style="margin-top: -12px;margin-bottom: 0px;">
                                                <a href="{{ url('aviso/' . $item->id) }}" class="name_avisos" style="font-size:16px;text-transform: uppercase;margin-bottom:0px">{{ $item->usuario->nombre}}  |  {{$item->usuario->edad}} a&ntilde;os</a>
                                            </h3>
                                            <h4 class="titulo_aviso" style="margin-bottom:5px;margin-top:5px;font-weight:700;"> {{ $item->titulo}}</h4>
                                            <p class="texto_contenido_aviso"> {{ substr($item->contenido, 0, 100) }} ...</p>
                                            <a href="{{ url('/modelo/' . $item->usuario->id . '/ver') }}"  class="boton_ver_perfil">
                                            Ver Perfil
                                        </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>

                    <div class="fw-divider-space hidden-below-lg mt-50"></div>
                </div>
            @endforeach

        </div>
    </section>
@endif


    
    
    @if(count($escorts) > 0)
        <section id="escorts" class="gallery-section gallery-6 bottom_mask_add overflow-visible ds s-pb-70 s-pb-md-80 s-pb-xl-155">
            <br><br>
            <h5 class="big-title" style="font-size: 50px;line-height: initial;text-align:center"> ESCORTS VIP EN TU CIUDAD</h5>
            <div class="container-fluid">
                <div class="row">
                    <div class="fw-divider-space hidden-below-xl pt-70"></div>
                    <div class="col-lg-12">
                        <div class="row isotope-wrapper masonry-layout c-gutter-5 c-mb-5 animate" data-animation="fadeInDown">
                            @foreach ($escorts as $item)
                                <div class="col-sm-3 col-lg-3 col-lgx-3 col-xl-3 col-xs-6 col-6">
                                    <div class="vertical-item item-gallery content-absolute text-center ds" >
                                        <a href="{{ url('/modelo/' . $item->id . '/ver') }}" class="item-media h-100 w-100 d-block">
                                            <img src="{{ asset('storage/' . $item->perfil_url) }}" alt="Modelo">
                                            <div class="media-links"></div>
                                        </a>
                                             @if($item->telefono!=0)
                                    <a href="https://api.whatsapp.com/send?phone={{ $item->telefono }}" target="_blank" style="background-color: transparent;"><img src="https://clubvip.cl/storage/watsapp_logo.png" style="width:40px;height:40px;margin-top:5px"></a>
                                     <a href="tel:+{{ $item->telefono }}" target="_blank" style="background-color: transparent;"><i class="fa fa-phone-square" aria-hidden="true" style="font-size:36px;color:#fff;float:right"></i></a>
                                     @endif
                                        <div class="item-content" style="background-color: #e90ba5;">
                                            
                                            <div class="item-title" style="top:-80px;width:100%">
                                                <div style="display:inline-flex"> <p style="font-size:14px">{{ $item->nombre }} </p> 
                                                </div> 
                                                <div> <p style="font-size:12px">{{ $item->nacionalidad }}, {{ $item->edad }}</p> </div>
                                                <div> <p style="font-size:10px">Medidas: {{ $item->busto }}-{{ $item->cintura }}-{{ $item->caderas }}</p> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

<!--**************************************ESCORT CON MEJOR VALORACION*************************************-->
  <section id="valoracion" class="gallery-section gallery-6 bottom_mask_add overflow-visible ds s-pb-70 s-pb-md-80 s-pb-xl-155" style="background:#222">
            <br><br>
            <h5 class="big-title" style="font-size: 50px;line-height: initial;text-align:center">ESCORT CON MEJOR VALORACI&OacuteN</h5>
            <div class="container-fluid">
                <div class="row">
                    <div class="fw-divider-space hidden-below-xl pt-70"></div>
                    <div class="col-lg-12">
                        <div class="row isotope-wrapper masonry-layout c-gutter-5 c-mb-5 animate" data-animation="fadeInDown">
                            @foreach ($escorts as $t)
                                <div class="col-sm-3 col-lg-3 col-lgx-3 col-xl-3 col-xs-6 col-6">
                                    <div class="vertical-item item-gallery content-absolute text-center ds" >
                                        <a href="{{ url('/modelo/' . $t->id . '/ver') }}" class="item-media h-100 w-100 d-block">
                                            <img src="{{ asset('storage/' . $t->perfil_url) }}" alt="Modelo">
                                            <div class="media-links"></div>
                                        </a>
                                             @if($t->telefono!=0)
                                    <a href="https://api.whatsapp.com/send?phone={{ $t->telefono }}" target="_blank" style="background-color: transparent;"><img src="https://clubvip.cl/storage/watsapp_logo.png" style="width:40px;height:40px;margin-top:5px"></a>
                                     <a href="tel:+{{ $t->telefono }}" target="_blank" style="background-color: transparent;"><i class="fa fa-phone-square" aria-hidden="true" style="font-size:36px;color:#fff;float:right"></i></a>
                                     @endif
                                        <div class="item-content" style="background-color: #e90ba5;">
                                            
                                            <div class="item-title" style="top:-80px;width:100%">
                                                <div style="display:inline-flex"> <p style="font-size:14px">{{ $t->nombre }} </p> 
                                                </div> 
                                                <div> <p style="font-size:12px">{{ $t->nacionalidad }}, {{ $t->edad }}</p> </div>
                                                <div> <p style="font-size:10px">Medidas: {{ $t->busto }}-{{ $t->cintura }}-{{ $t->caderas }}</p> </div>
                                                @foreach ($valoracion as $valora)
                                                @if($t->id == $valora->id_usuario)
                                                 @if($valora->nota_final==5)
                                                <!--<div><img src="https://tusencuentros.cl/storage/5_1_estrellas.png" style="height: fit-content;"></div>-->
                              <div style="display:inline-flex;color:yellow"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                                                @endif
                                                @if($valora->nota_final==6)
                                              <!--  <div><img src="https://tusencuentros.cl/storage/6_1_estrellas.png" style="height: fit-content;"></div>-->
                              <div style="display:inline-flex;color:yellow"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                                                @endif
                                                 @if($valora->nota_final==7)
                                                <!--<div><img src="https://tusencuentros.cl/storage/7_1_estrellas.png" style="height: fit-content;"></div>-->
                                                <div style="display:inline-flex;color:yellow" class="iconos_estrellas"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                                                @endif
                                                @endif
                                                @endforeach
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>  
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!--******************************************************************************************************-->

   <!-- <div class="row" style="background: #1a1a1a;">
        <a href="https://www.clubvip.cl/" class="col-md-12">
            <img src="{{ asset('principal/images/slider_home.jpeg') }}" class="img-responsive center-block" style="auto" alt="clubvip.cl">
        </a>
    </div>-->

    @if(count($avisos) > 0)
    <section id="avisos" class="ds s-pt-70 s-pb-20 s-pb-sm-50 s-py-lg-100 s-py-xl-150">
        <br><br>
      <h5 class="big-title" style="font-size: 50px;line-height: initial;text-align:center">AVISOS ER&OacuteTICOS</h5>
       <section id="publicateEscort" style="background:transparent;">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-xl-4 animate" data-animation="scaleAppear">
                    <h2 class="mt-0 mb-40 contact-title text-uppercase">PUBLICATE EN</h2>
                    <span class="color-main fs-24 font-main text-uppercase">TUSENCUENTROS.CL</span>
                    
                    <div class="media mb-20">
                        <h5 class="fs-20 mb-0 min-w-100">Email:</h5>
                        <div class="media-body ml-0 d-flex flex-column">
                            <span>contacto@tusencuentros.cl</span>
                        </div>
                    </div>
                </div>
                <!--.col-* -->
                <div class="fw-divider-space hidden-above-lg mt-20"></div>
                <div class="col-lg-7 col-xl-8 animate" data-animation="scaleAppear">                                
                    <form class="contact-form c-mb-20 c-gutter-20" method="POST" action="{{ route('register') }}">
                        @csrf
                        <input type="hidden" id="tipo_usuario" name="tipo_usuario" value="2">
    
                        @if($errors->any())
                            <div class="row">
                                <div class="col-12">
                                    <div class="alert alert-danger text-left">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="usuario" class="form-control" placeholder="Usuario">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="password" name="clave" class="form-control" placeholder="Contraseña">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select name="sexo" id="sexo" class="form-control" style="height: 60px;">
                                        <option value="1" selected>MUJER</option>
                                        <option value="2">HOMBRE</option>
                                    </select>
                                    <button type="submit" class="btn-submit"><i class="fa fa-paper-plane"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--.col-* -->
            </div>
        </div>
    </section>
   
        
    </section>
@endif


   
        <section id="todas" class="gallery-section gallery-6 bottom_mask_add overflow-visible ds s-pb-70 s-pb-md-80 s-pb-xl-155">
            <br><br>
            <h5 class="big-title" style="font-size: 50px;line-height: initial;text-align:center">TODAS LAS ESCORT</h5>
            <div class="container-fluid">
                <div class="row">
                    <div class="fw-divider-space hidden-below-xl pt-70"></div>
                    <div class="col-lg-12">
                        <div class="row isotope-wrapper masonry-layout c-gutter-5 c-mb-5 animate" data-animation="fadeInDown">
                            @foreach ($todas as $t)
                                <div class="col-sm-3 col-lg-3 col-lgx-3 col-xl-3 col-xs-6 col-6">
                                    <div class="vertical-item item-gallery content-absolute text-center ds" >
                                        <a href="{{ url('/modelo/' . $t->id . '/ver') }}" class="item-media h-100 w-100 d-block">
                                            <img src="{{ asset('storage/' . $t->perfil_url) }}" alt="Modelo">
                                            <div class="media-links"></div>
                                        </a>
                                             @if($t->telefono!=0)
                                    <a href="https://api.whatsapp.com/send?phone={{ $t->telefono }}" target="_blank" style="background-color: transparent;"><img src="https://clubvip.cl/storage/watsapp_logo.png" style="width:40px;height:40px;margin-top:5px"></a>
                                     <a href="tel:+{{ $t->telefono }}" target="_blank" style="background-color: transparent;"><i class="fa fa-phone-square" aria-hidden="true" style="font-size:36px;color:#fff;float:right"></i></a>
                                     @endif
                                        <div class="item-content" style="background-color: #e90ba5;">
                                            
                                            <div class="item-title" style="top:-80px;width:100%">
                                                <div style="display:inline-flex"> <p style="font-size:14px">{{ $t->nombre }} </p> 
                                                </div> 
                                                <div> <p style="font-size:12px">{{ $t->nacionalidad }}, {{ $t->edad }}</p> </div>
                                                <div> <p style="font-size:10px">Medidas: {{ $t->busto }}-{{ $t->cintura }}-{{ $t->caderas }}</p> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
   
	<script src="https://kit.fontawesome.com/f755b555f3.js"></script>

@endsection
