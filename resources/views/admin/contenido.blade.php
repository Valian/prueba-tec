@extends('admin')

@section('content')
    <template v-if="menu==0">
        <usuarios></usuarios>
    </template>

    <template v-if="menu==1">
        <pagina></pagina>
    </template>

    <template v-if="menu==2">
        <blog></blog>
    </template>
@endsection