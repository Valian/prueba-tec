@extends('admin')

@section('content')
    <input type="hidden" id="id_usuario" name="id_usuario" value="{{ $usuario_id }}">
    
    <template v-if="menu==0">
        <perfil-usuario-admin></perfil-usuario-admin>
    </template>

    <template v-if="menu==1">
        <pagina></pagina>
    </template>
@endsection