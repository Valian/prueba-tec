<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--<![endif]-->

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
	
	{!! SEO::generate() !!}
	
    <meta name="robots" content="Index, Follow">
    
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('titulo', 'Las mejores escort en tusencuentros.cl')</title>

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css.br') }}" rel="stylesheet">
    <link href="{{ asset('css/principal.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
	<!-- <link rel="preload" href="{{ asset('css/free.min.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
	<noscript><link rel="stylesheet" href="{{ asset('css/free.min.css') }}"></noscript> -->
	<!--[if lt IE 9]>
        <script src="js/vendor/html5shiv.min.js"></script>
        <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144758712-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144758712-1');
</script>

</head>

<body style="background-color: #000;">
	<!--[if lt IE 9]>
        <div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
    <![endif]-->
    <style>
        
    @media  screen and (min-width: 993px) {
        .margen_link_menu{
	    
	    margin-left:20px;
	}
        
    }
    @media  screen and (max-width: 992px) {
	.page_header_side .header-phone {
	display: block;
    font-size: 9px;
    margin-top: 85px;
    letter-spacing: 3px;
    margin-left: -230px;
	}
	.margen_link_menu{
	    
	    margin-left:10px;
	}
	.imagefondo{
	background-image:url({{ asset('principal/images/home_1_2.webp') }})!important;
	}
    }

    </style>
    <div id="app">
	    <!-- wrappers for visual page editor and boxed version of template -->
	    <div id="canvas">
            <div id="box_wrapper">
				<div id="selectorandroid" class="header_absolute" style="height: 100vh;">
					<header class="page_header page_header_side vertical_menu_header ds bottom_mask_add">
						<div class="container-fluid">
                            <div class="row">
								<div class="col-12 my-0 mx-0 d-flex justify-content-between align-items-center" style="height: 40px;">
									<a href="{{ url('/') }}" class="logo"  style="height: 40px;">
										<img src="{{ asset('principal/images/logo.webp') }}"  class="m-auto pt-2" alt="img" style="height: 50px;">
									</a>
									<span class="header-phone col-xs-8" style="text-shadow: 2px 2px 3px #000;">
										<ul class="menu_web" style="display: flex; list-style: none;">
											<li class="margen_link_menu"><a href="#">Escorts</a></li>
											<li class="margen_link_menu"><a href="#">Eventos</a></li>
											<li class="margen_link_menu"><a href="#">Gays</a></li>
											<li class="margen_link_menu"><a href="#">Lesbico</a></li>
											<li class="margen_link_menu"><a href="#">Heteros</a></li>
										</ul>
									</span>
									<span class="header-soc my-auto pt-3">
											@guest
												<a href="{{ route('login') }}"><i class="fa fa-user" aria-hidden="true"></i></a>
											@else
												<a href="{{ route('login') }}"><i class="fa fa-user" aria-hidden="true"></i> {{ Auth::user()->nombre }}</a>
											@endguest						
											<!-- <span class="toggle_menu toggle_menu_side header-slide toggle_menu_special"><span> -->
									</span>
								</div>
							</div>

						</div>
					</header>
					<section class="page_slider cover-image ds text-center bottom_mask_subtract imagefondo">
						<img src="{{ asset('principal/images/home_1.jpg') }}" class="" alt="img">

						<div class="container-fluid">
							<div class="row">
								<div class="col-md-12">
									<div class="intro_layers_wrapper">
										<div class="intro_layers">
											<div class="intro_layer mt-30" data-animation="fadeInUp" style="padding-bottom: 20px;">
												<div class="intro_after_featured_word" style="text-shadow: 0 2px 6px black;">
													<h3>Selecciona tu ciudad</h3>
												</div>
											</div>
											
											<div class="intro_layer" data-animation="fadeInRight">
												<div class="d-inline-block">
												    <i class="fa fa-arrow-down" aria-hidden="true"></i>
													<select name="ciudad_id" id="ciudad_id" class="form-control"onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);" style="
                                                        height: 80%;
                                                        width: 15rem;
                                                        margin: auto;
                                                        border-radius: 40px;
                                                        box-shadow: 0px 3px 8px 0px #00000047;">
													    <i class="fa fa-arrow-down" aria-hidden="true"></i>
														@foreach ($ciudades as $item)
															<option value="{{ url('/ciudad/' . $item->id) }}">{{ $item->nombre }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div> <!-- eof .intro_layers -->
									</div> <!-- eof .intro_layers_wrapper -->
								</div> <!-- eof .col-* -->
							</div><!-- eof .row -->
						</div><!-- eof .container-fluid -->
					</section>
				</div>

				<footer class="page_footer col-12" style="color: white;margin-top: -40px;z-index: 30000000;background-color: transparent;position: absolute;">
					<div class="container-fluid p-0">
						<div class="row">
							<div class="col-12 text-center animate" data-animation="fadeInUp">
								<div class="widget copyright mb-0" style="font-size: 14px;">
									<p>&copy; <span class="copyright_year">2018</span> Todos los derechos reservados.</p>
								</div>
							</div>

						</div>
					</div>
				</footer>

            </div>
            <!-- eof #box_wrapper -->
        </div>
        <!-- eof #canvas -->
    </div>


    <script src="{{ asset('js/app.js.br') }}"></script>
    <script src="{{ asset('js/principal.js') }}"></script>
 
	<!-- <script src="https://kit.fontawesome.com/f755b555f3.js"></script> -->

	<!-- <script src="https://www.tusencuentros.cl/js/script.js"></script> -->

</body>

</html>