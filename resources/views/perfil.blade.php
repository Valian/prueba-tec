@extends('layouts.modelo')

@section('contenido')

				<div class="header_absolute">
					<header class="page_header page_header_side vertical_menu_header ds bottom_mask_add"><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
						<div class="container-fluid">
                            <div class="row">
								<div class="col-12 my-0 mx-0 d-flex justify-content-between align-items-center" style="height: 40px;">
									<a href="{{ url('/') }}" class="logo"  style="height: 40px;">
										<img src="{{ asset('principal/images/logo.png') }}"  class="m-auto pt-2" alt="img" style="height: 50px;">
									</a>
									<span class="header-phone m-auto pt-2" style="text-shadow: 2px 2px 3px #000;">
										<ul class="menu_web" style="display: flex; list-style: none;">
											<li class="margen_link_menu m-2"><a href="#"><strong>Escorts</strong></a></li>
											<li class="margen_link_menu m-2"><a href="#">Eventos</a></li>
											<li class="margen_link_menu m-2"><a href="#">Gays</a></li>
											<li class="margen_link_menu m-2"><a href="#">Lesbianas</a></li>
											<li class="margen_link_menu m-2"><a href="#">Heteros</a></li>
										</ul>
									</span>
									<span class="header-soc my-auto pt-3">
											@guest
												<a href="{{ route('login') }}"><i class="fa fa-user" aria-hidden="true"></i></a>
											@else
												<a href="{{ route('login') }}"><i class="fa fa-user" aria-hidden="true"></i> {{ Auth::user()->nombre }}</a>
											@endguest
									</span>
								</div>
							</div>
						</div>
					</header>
				</div>

    <section class="ds single-model s-pt-50 s-pb-20 s-pb-sm-20 s-py-lg-100 s-py-xl-70 c-mb-30 c-gutter-50">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 style="text-transform: uppercase;">{{ $usuario->nombre . ' - ' . $usuario->telefono}}</h1>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ url('/') }}}">Inicio</a>
                        </li>
                        <li class="breadcrumb-item active" style="text-transform: uppercase;">
                            {{ $usuario->nombre}}
                        </li>
                    </ol>
                </div>

                <div class="fw-divider-space hidden-above-lg mt-0"></div>

            </div>
            <div class="row">
                <div class="col-xs-12 col-lg-6">

                    <div class="vertical-item content-padding item-gallery">
                        <div class="model border-rad-5 pb-0">
                            <div class="model-images">
                                <div class="model-figure">
                                    <div data-thumb="{{ asset('storage/' . $usuario->perfil_url) }}">
                                        <a href="{{ asset('storage/' . $usuario->perfil_url) }}">
                                            <img src="{{ asset('storage/' . $usuario->perfil_url) }}" alt="Foto Perfil" data-caption="" data-large_image_width="350" data-large_image_height="350" style="margin: auto;display: block;max-height: 500px;">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="item-content" style="top: 0px;">
                                <ul class="model-data">
                                    <li>
                                        <span class="title">Edad</span>
                                        <span class="data">{{ $usuario->edad }}</span>
                                    </li>
                                    <li>
                                        <span class="title">Altura</span>
                                        <span class="data">{{ $usuario->altura }}</span>
                                    </li>
                                    <li>
                                        <span class="title">Busto</span>
                                        <span class="data">{{ $usuario->busto }}</span>
                                    </li>
                                    <li>
                                        <span class="title">Cintura</span>
                                        <span class="data">{{ $usuario->cintura }}</span>
                                    </li>
                                    <li>
                                        <span class="title">Caderas</span>
                                        <span class="data">{{ $usuario->caderas }}</span>
                                    </li>
                                </ul>
                            </div>                       
                        </div>
                    </div>

                </div>

                <div class="col-xs-12 col-lg-6">
                    <h2 class="entry-title" style="text-transform: uppercase;">{{ $usuario->nombre . ' - ' . $usuario->nacionalidad }}</h2>
                    <div class="fw-divider-space hidden-below-lg mt--10"></div>
                    <br><br>
                    <p>
                        {{ $usuario->descripcion }}
                    </p>

                    @if($usuario->video_url != null)
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="{{ asset('storage/' . $usuario->video_url) }}" allowfullscreen autoplay></iframe>
                        </div>
                    @endif
                </div>
            </div>

            @if(count($usuario->urls) > 0)
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row  c-mb-30" >
                            @foreach ($usuario->urls as $item)
                               <!-- <div class="col-xl-3 col-sm-6 fashion">-->
                               <div>
                                    <div class="vertical-item item-gallery text-center ds">
                                        <div class="item-media h-100 w-100 d-block">
                                            <img src="{{ asset('storage/' . $item) }}" alt="img" style="height:auto">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif

        </div>
        <div class="fw-divider-space hidden-below-lg mt-25"></div>
        <div class="fw-divider-space hidden-xs hidden-above-lg mt-30"></div>
    </section>

    @if(count($videos) > 0)
    <section class="ds s-pt-70 s-pb-50 s-pb-sm-70 s-py-lg-100 s-py-xl-150 c-gutter-60">
        <div class="container">
            @foreach($videos AS $item)
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="item-media post-thumbnail pb-40">
                            <div class="embed-responsive embed-responsive-3by2">
                                <a href="{{ asset('storage/' . $item->video_url) }}" class="embed-placeholder">
                                    <img src="{{ asset('storage/' . $item->video_url) }}" alt="img">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="fw-divider-space hidden-below-lg mt-50"></div>
    </section>
    @endif
    
     <!----------------------SISTEMA DE VALORACION------------------------------>
    <section class="ds s-pt-70 s-pb-50 s-pb-sm-70 s-py-lg-100 s-py-xl-150 c-gutter-40">
        <div class="container">
            <h1 style="text-transform: uppercase;text-align:center;color:#e41779">Calif&iacuteca a {{ $usuario->nombre}}</h1>
            <h2 style="text-align:center">&iquestTe atendiste con ella?</h2>
            <form class="contact-form c-mb-20 c-gutter-20" method="POST" action="/calificar">
                       {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                     <input type="number" name="id_usuario" value="{{ $usuario->id}}" style="display:none">
                                </div>
                            </div>
                          
                        </div>
                        <div class="row">
                            <label>Atenci&oacuten</label>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    
                                    <select name="atencion" id="atencion" class="form-control" style="height: 60px;">
                                        <option value="" selected>Seleccione opci&oacuten</option>
                                        <option value="Bajo">Bajo</option>
                                        <option value="Regular">Regular</option>
                                         <option value="Optimo">&Oacuteptimo</option>
                                    </select>
                           
                                </div>
                            </div>
                        </div>
                            <div class="row">
                                <label>Higiene</label>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    
                                    <select name="higiene" id="higiene" class="form-control" style="height: 60px;">
                                       <option value="" selected>Seleccione opci&oacuten</option>
                                        <option value="Bajo">Bajo</option>
                                        <option value="Regular">Regular</option>
                                         <option value="Optimo">&Oacuteptimo</option>
                                    </select>
                           
                                </div>
                            </div>
                        </div>
                          <div class="row">
                               <label>Fotos</label>
                            <div class="col-sm-12">
                                <div class="form-group">
                                   
                                    <select name="fotos" id="fotos" class="form-control" style="height: 60px;">
                                        <option value="" selected>Seleccione opci&oacuten</option>
                                        <option value="Bajo">Bajo</option>
                                        <option value="Regular">Regular</option>
                                         <option value="Optimo">&Oacuteptimo</option>
                                    </select>
                           
                                </div>
                            </div>
                        </div>
                        
                             <div class="row">
                                   <label>Nota Final</label>
                            <div class="col-sm-12">
                                <div class="form-group">
                                  
                                        <div class="clasificacion" style="margin-left:-30px">
  <input id="radio1" type="radio" name="nota_final" value="1" >
  <label for="radio1"><i class="fa fa-star"></i></label>
  <input id="radio2" type="radio" name="nota_final" value="2">
  <label for="radio2"><i class="fa fa-star"></i></label>
  <input id="radio3" type="radio" name="nota_final" value="3">
  <label for="radio3"><i class="fa fa-star"></i></label>
  <input id="radio4" type="radio" name="nota_final" value="4">
  <label for="radio4"><i class="fa fa-star"></i></label>
  <input id="radio5" type="radio" name="nota_final" value="5">
  <label for="radio5"><i class="fa fa-star"></i></label>
  <input id="radio6" type="radio" name="nota_final" value="6">
  <label for="radio6"><i class="fa fa-star"></i></label>
  <input id="radio7" type="radio" name="nota_final" value="7">
  <label for="radio7"><i class="fa fa-star"></i></label>
</div>
                           
                                </div>
                            </div>
                        </div>
                         <div class="row" style="margin-top:100px">
                            <div class="col-sm-12">
                                <div class="form-group">
                                   
                                    <button type="submit" class="btn-submit"><i class="fa fa-paper-plane"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
            
            
                
                
                <style>
                            .clasificacion {
  position: relative;
  overflow: hidden;
  display: inline-block;
}

.clasificacion input {
  position: absolute;
  top: -100px;
}

.clasificacion label {
  float: right;
  color: #333;
}

.clasificacion label:hover,
.clasificacion label:hover ~ label,
.clasificacion input:checked ~ label {
  color: #dd4;
}
.clasificacion input[type=radio]+label:before {
    border: 0px solid #e1e1e1;
    border-radius: 0%;
    
}

.clasificacion input[type=radio]:checked+label:before {
  
    background-color: transparent;
  
}
                       
                        </style>
                        
                        
  
        </div>
        <div class="fw-divider-space hidden-below-lg mt-50"></div>
    </section>
    
   
   
   
   
   
     <!----------------------SISTEMA DE VALORACION CLIENTES------------------------------>
   @if(auth()->user())
   
    <section class="ds s-pt-70 s-pb-50 s-pb-sm-70 s-py-lg-100 s-py-xl-150 c-gutter-40">
        <div class="container">
            <h1 style="text-transform: uppercase;text-align:center;color:#e41779">Calif&iacuteca al cliente</h1>
            <h2 style="text-align:center">&iquestEstuviste con &eacutel?</h2>
    
            <form class="contact-form c-mb-20 c-gutter-20" method="POST" action="/calificar/cliente">
                       {{ csrf_field() }}
                        <div class="row">
                              <label>Nickname</label>
                            <div class="col-sm-6">
                                <div class="form-group">
                                     <input type="text" name="nickname" >
                                </div>
                            </div>
                          
                        </div>
                     
                        <div class="row">
                            <label>Higiene</label>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    
                                    <select name="higiene" id="higiene" class="form-control" style="height: 60px;">
                                        <option value="" selected>Seleccione opci&oacuten</option>
                                        <option value="Bajo">Bajo</option>
                                        <option value="Regular">Regular</option>
                                         <option value="Optimo">&Oacuteptimo</option>
                                    </select>
                           
                                </div>
                            </div>
                        </div>
                            <div class="row">
                                <label>Respeto</label>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    
                                    <select name="respeto" id="respeto" class="form-control" style="height: 60px;">
                                       <option value="" selected>Seleccione opci&oacuten</option>
                                        <option value="Bajo">Bajo</option>
                                        <option value="Regular">Regular</option>
                                         <option value="Optimo">&Oacuteptimo</option>
                                    </select>
                           
                                </div>
                            </div>
                        </div>
                          <div class="row">
                               <label>Dinero</label>
                            <div class="col-sm-12">
                                <div class="form-group">
                                   
                                    <select name="dinero" id="dinero" class="form-control" style="height: 60px;">
                                        <option value="" selected>Seleccione opci&oacuten</option>
                                        <option value="Bajo">Bajo</option>
                                        <option value="Regular">Regular</option>
                                         <option value="Optimo">&Oacuteptimo</option>
                                    </select>
                           
                                </div>
                            </div>
                        </div>
                        
                             <div class="row">
                                   <label>Nota Final</label>
                            <div class="col-sm-12">
                                <div class="form-group">
                                  
                                        <div class="clasificacion_cliente" style="margin-left:-30px">
  <input id="radio1_cliente" type="radio" name="nota_final" value="1" >
  <label for="radio1_cliente"><i class="fa fa-star"></i></label>
  <input id="radio2_cliente" type="radio" name="nota_final" value="2">
  <label for="radio2_cliente"><i class="fa fa-star"></i></label>
  <input id="radio3_cliente" type="radio" name="nota_final" value="3">
  <label for="radio3_cliente"><i class="fa fa-star"></i></label>
  <input id="radio4_cliente" type="radio" name="nota_final" value="4">
  <label for="radio4_cliente"><i class="fa fa-star"></i></label>
  <input id="radio5_cliente" type="radio" name="nota_final" value="5">
  <label for="radio5_cliente"><i class="fa fa-star"></i></label>
  <input id="radio6_cliente" type="radio" name="nota_final" value="6">
  <label for="radio6_cliente"><i class="fa fa-star"></i></label>
  <input id="radio7_cliente" type="radio" name="nota_final" value="7">
  <label for="radio7_cliente"><i class="fa fa-star"></i></label>
</div>
                           
                                </div>
                            </div>
                        </div>
                        
                           <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                     <input type="number" name="id_modelo" value="{{ $usuario->id}}" style="display:none">
                                </div>
                            </div>
                          
                        </div>
                         <div class="row" style="margin-top:100px">
                            <div class="col-sm-12">
                                <div class="form-group">
                                   
                                    <button type="submit" class="btn-submit"><i class="fa fa-paper-plane"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
            
            
                
                
                <style>
                            .clasificacion_cliente {
  position: relative;
  overflow: hidden;
  display: inline-block;
}

.clasificacion_cliente input {
  position: absolute;
  top: -100px;
}

.clasificacion_cliente label {
  float: right;
  color: #333;
}

.clasificacion_cliente label:hover,
.clasificacion_cliente label:hover ~ label,
.clasificacion_cliente input:checked ~ label {
  color: #dd4;
}
.clasificacion_cliente input[type=radio]+label:before {
    border: 0px solid #e1e1e1;
    border-radius: 0%;
    
}

.clasificacion_cliente input[type=radio]:checked+label:before {
  
    background-color: transparent;
  
}
                       
                        </style>
                        
                        
  
        </div>
        <div class="fw-divider-space hidden-below-lg mt-50"></div>
    </section> 
    @endif
    	<script src="https://kit.fontawesome.com/f755b555f3.js"></script>

@endsection
