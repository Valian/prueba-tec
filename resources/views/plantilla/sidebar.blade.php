<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">--- MENÚ</li>
                <li @click="menu=0"> <a class="waves-effect waves-dark" href="javascript::void(0)" aria-expanded="false"><i class="fa fa-line-chart"></i><span class="hide-menu">MI PERFIL</span></a></li>
                @if(Auth::user()->tipo == 2) <li @click="menu=1"> <a class="waves-effect waves-dark" href="javascript::void(0)" aria-expanded="false"><i class="fa fa-list"></i><span class="hide-menu">MIS ESTADOS</span></a></li>@endif
                <li @click="menu=1"> <a class="waves-effect waves-dark" href="javascript::void(0)" aria-expanded="false"><i class="fa fa-file"></i><span class="hide-menu">MIS AVISOS</span></a></li>
                <li> <a class="waves-effect waves-dark" href="{{ route('logout') }}" aria-expanded="false" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> <span class="hide-menu">CERRAR SESIÓN</span> </a><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form></li>
            </ul>
        </nav>
    </div>
</aside>
    
