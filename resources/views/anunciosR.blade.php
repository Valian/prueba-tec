@extends('layouts.principal')

@section('contenido')

    <header class="page_header header_darkgrey columns_padding_0 table_section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-sm-6 text-center">
                    <a href="{{ url('/escorts') }}" class="logo logo_image">
                        <img src="{{ asset("principal/images/logo.png") }}" alt="" style="max-height: 60px;">
                    </a>
                </div>
                <div class="col-md-6 text-center">
                    <!-- main nav start -->
                    <nav class="mainmenu_wrapper">
                        <ul class="center-block mainmenu nav sf-menu">
                            <li class="active">
                                <a href="#inicio">Inicio</a>
                            </li>
                            
                            <li>
                                <a href="#avisos">Avisos</a>
                            </li>

                            <li>
                                <a href="#publicate">Publica tu aviso</a>
                            </li>
                        </ul>
                    </nav>
                    <!-- eof main nav -->
                    <span class="toggle_menu">
                        <span></span>
                    </span>
                </div>
                <div class="col-md-3 col-sm-6 header-contacts text-center hidden-xs">
                    <div class="highlight inline-block fontsize_30 thin"><a title="Click para chatear" href="">+56 {{ $pagina->telefono }} <i class="fab fa-whatsapp"></i></a></div>
                    <div class="fontsize_20 grey topmargin_-5">Concepción</div>
                </div>
            </div>
        </div>
    </header>

    <section id="publicate" class="ds section_padding_70 parallax" style="background-image: url('../../principal/images/parallax/map2.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-3 col-sm-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6 text-center">
                    <h2 class="big margin_0">Publica ahora ya</h2>
                    <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">Sin esperas</h2>

                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <input type="hidden" id="tipo_usuario" name="tipo_usuario" value="3">

                        @if($errors->any())
                            <div class="row">
                                <div class="col-12">
                                    <div class="alert alert-danger text-left">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif
                        
                        <div class="form-group">
                            <label for="usuario" class="sr-only">Usuario
                                <span class="required">*</span>
                            </label>
                            <input type="text" aria-required="true" size="30" value="" name="usuario" id="usuario" class="form-control text-center" placeholder="Usuario" required value="{{ old('email') }}">
                        </div>
                        <div class="form-group">
                            <label for="name" class="sr-only">Contraseña
                                <span class="required">*</span>
                            </label>
                            <input type="password" aria-required="true" size="30" value="" name="clave" id="clave" class="form-control text-center" placeholder="Contraseña" required>
                        </div>
                        <div class="form-group">
                            <label for="email" class="sr-only">Confirmar contraseña
                                <span class="required">*</span>
                            </label>
                            <select name="sexo" id="sexo" class="form-control">
                                <option value="1">Mujer</option>
                                <option value="2">Hombre</option>
                            </select>
                        </div>
                        <br>
                        <button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button color1 bottommargin_0">Registrarme</button>
                        <button type="reset" id="contact_form_clear" name="contact_clear" class="theme_button inverse bottommargin_0">Limpiar</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="avisos" class="ds section_padding_70 section_padding_bottom_50">
        <div class="container">
            <div class="row bottommargin_20">
                <div class="col-sm-12 text-center">
                    <h2 class="big margin_0">¿Buscas algo o deseas publicar?</h2>
                    <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">Avisos eróticos</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="panel-group">
                        @foreach($avisos as $item)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="{{ url('/aviso/' . $item->id) }}">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3 text-center">
                                                    <img src="{{ asset('storage/' . $item->usuario->perfil_url) }}" alt="..." class="img-fluid" width="120px">
                                                </div>
                                                <div class="col-md-9 col-sm-3 text-left">
                                                    <h3>{{ $item->titulo }}</h3>
                                                    <p>{{ substr($item->contenido, 0, 200) }} ... Ver más aquí</p>
                                                </div>
                                            </div>
                                        </a>
                                    </h4>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </section>

    <div class="row" style="background: #1a1a1a;">
        <a href="https://www.escortconce.cl/" class="col-md-12">
            <img src="{{ asset('principal/images/slider_1.jpeg') }}" class="img-responsive center-block" style="width: 80%;" alt="escortconce.cl">
        </a>
    </div>
@endsection
