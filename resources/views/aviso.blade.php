@extends('layouts.modelo')

@section('contenido')

    <section class="page_breadcrumbs changeable ls gradient gorizontal_padding section_padding_20 columns_padding_5 table_section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 text-center text-sm-left darklinks">
                    <a href="#">
                        <em>{{ $aviso->usuario->telefono }}</em>
                    </a>
                </div>
                <div class="col-sm-6 text-center">
                    <ol class="center-block breadcrumb">
                        <li>
                            <a href="{{ url('/escorts') }}">
                                Inicio
                            </a>
                        </li>
                        <li class="active">
                            <span> Aviso de {{ $aviso->usuario->nombre}}</span>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-3 text-center text-sm-right">
                    <ul class="inline-dropdown inline-block">
                        <li class="dropdown login-dropdown">
                            @guest
								<li class="dropdown login-dropdown">
									<a class="topline-button" id="login" data-target="#" href="./" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
										<i class="rt-icon2-user"></i>
									</a>
									<div class="dropdown-menu ds" aria-labelledby="login">
                                        <form role="form" method="POST" action="{{ route('login') }}">
                                            @csrf
											<div class="form-group">
												<label for="email" class="sr-only">Email</label>
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
											</div>
											<div class="form-group">
												<label for="password" class="sr-only">Contraseña</label>
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
											</div>
											<button type="submit" class="theme_button color1 bottommargin_0">
												Ingresar
											</button>
											<div class="checkbox-inline">
												<input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label for="remember" class="bottommargin_0"> Recuérdame</label>
											</div>
										</form>
									</div>
                                </li>                                  
                            @else
                                <a class="topline-button" id="login" href="{{ url('/home')}}" role="button" >
                                    <i class="rt-icon2-user"></i> {{ Auth::user()->nombre }}
                                </a>
                            @endguest
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="ds model-page section_padding_70 section_padding_bottom_60 columns_padding_25">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-push-7">
                    <div class="vertical-item with_background models_square">
                        <div class="item-media">
                            <img src="{{ asset('storage/' . $aviso->usuario->perfil_url) }}" alt="Modelo">
                        </div>
                    </div>

                </div>

                <div class="col-md-7 col-md-pull-5">
                    <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">{{ $aviso->titulo }}</h2>
                    <h3></h3>
                    <p>
                        {!! $aviso->contenido !!}
                    </p>
                </div>

            </div>

            @if(count($aviso->usuario->urls) > 0)
                <div class="row">
                    <div class="col-md-12">
                        <div id="gallery-owl-carousel" class="owl-carousel" data-dots="true" data-items="3" data-responsive-lg="3" data-responsive-md="2" data-responsive-sm="2" data-responsive-xs="1">
                            @foreach ($aviso->usuario->urls as $item)
                                <div class="vertical-item gallery-extended-item with_background text-center">
                                    <div class="item-media text-center">
                                        <img src="{{ asset('storage/' . $item) }}" class="img-fluid" alt="Modelo" height="350px">
                                    </div>
                                </div>               
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>

@endsection
