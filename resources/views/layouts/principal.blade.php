<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--<![endif]-->

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
	
	{!! SEO::generate() !!}
	
    <meta name="robots" content="Index, Follow">
    
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('titulo', 'Las mejores escort en tusencuentros.cl')</title>

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/principal.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
<link rel="stylesheet" href="https://kit-free.fontawesome.com/releases/latest/css/free.min.css" media="all">
	<!--[if lt IE 9]>
        <script src="js/vendor/html5shiv.min.js"></script>
        <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144758712-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144758712-1');
</script>

</head>

<body style="background-color: #000;">
	<!--[if lt IE 9]>
        <div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
    <![endif]-->
    <style>
        
    @media  screen and (min-width: 993px) {
        .margen_link_menu{
	    
	    margin-left:20px;
	}
        
    }
    @media  screen and (max-width: 992px) {
	.page_header_side .header-phone {
	display: block;
    font-size: 9px;
    margin-top: 85px;
    letter-spacing: 3px;
    margin-left: -230px;
	}
	.margen_link_menu{
	    
	    margin-left:10px;
	}
    }

    </style>
    <div id="app">
	    <!-- wrappers for visual page editor and boxed version of template -->
	    <div id="canvas">
            <div id="box_wrapper">
				<div class="header_absolute">
					<header class="page_header page_header_side vertical_menu_header ds bottom_mask_add">
						<div class="container-fluid">
                            <div class="row">
								<div class="col-12 my-0 mx-0 d-flex justify-content-between align-items-center" style="height: 40px;">
									<a href="{{ url('/') }}" class="logo"  style="height: 40px;">
										<img src="{{ asset('principal/images/logo.png') }}"  class="m-auto pt-2" alt="img" style="height: 50px;">
									</a>
									<span class="header-phone col-xs-8" style="text-shadow: 2px 2px 3px #000;">
										<ul class="menu_web" style="display: flex; list-style: none;">
											<li class="margen_link_menu m-2"><a href="#"><strong>Escorts</strong></a></li>
											<li class="margen_link_menu m-2"><a href="#">Eventos</a></li>
											<li class="margen_link_menu m-2"><a href="#">Gays</a></li>
											<li class="margen_link_menu m-2"><a href="#">Lesbico</a></li>
											<li class="margen_link_menu m-2"><a href="#">Heteros</a></li>
										</ul>
									</span>
									<span class="header-soc my-auto pt-3">
											@guest
												<a href="{{ route('login') }}"><i class="fa fa-user" aria-hidden="true"></i></a>
											@else
												<a href="{{ route('login') }}"><i class="fa fa-user" aria-hidden="true"></i> {{ Auth::user()->nombre }}</a>
											@endguest
									</span>
								</div>
							</div>
						</div>
					</header>
				</div>
                @yield('contenido')
				<footer class="page_footer ds top_mask_add s-pb-10 s-pt-70 s-pb-md-40 s-pt-md-85 s-pb-xl-0 s-pt-xl-30 s-pb-sm-0 s-pt-sm-0">
					<div class="container">
						<div class="row">
							<div class="divider-20 d-none d-xl-block"></div>

							<div class="col-12 text-center animate" data-animation="fadeInUp">

								<div class="widget copyright">
									<p>&copy; <span class="copyright_year">2018</span> Todos los derechos reservados.</p>
								</div>
							</div>

						</div>
					</div>
				</footer>

            </div>
            <!-- eof #box_wrapper -->
        </div>
        <!-- eof #canvas -->
    </div>


    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/principal.js') }}"></script>
 
	<script src="https://kit.fontawesome.com/f755b555f3.js"></script>
</body>

</html>