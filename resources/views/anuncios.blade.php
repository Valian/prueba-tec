@extends('layouts.modelo')

@section('contenido')


    <header class="page_header ds bottom_mask_add"><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-xl-3 col-lg-4 col-md-5 col-11">
                      <a href="{{ url('/') }}" class="logo logo_image">
                        <img src="{{ asset("principal/images/logo.png") }}" alt="" style="max-height: 60px;">
                    </a>
                </div>
                <div class="col-xl-6 col-lg-8 col-md-7 col-1">
                    <div class="nav-wrap">

                        <!-- main nav start -->
                        <nav class="top-nav">
                            <ul class="nav sf-menu">
                                <li class="active"><a href="#">Inicio</a></li>
                                <li><a href="#">Nosotros</a></li>
                                <li><a href="#">Agencia</a></li>
                            </ul>
                        </nav>
                        <!-- eof main nav -->

                        <!--hidding includes on small devices. They are duplicated in topline-->

                    </div>
                </div>
               
            </div>
        </div>
        <!-- header toggler -->
        <span class="toggle_menu"><span></span></span>
    </header>
    
         <section id="publicateEscort" class="contacts-section top_mask_add ds overflow-visible background-contact s-pt-70 s-pb-60 s-pt-md-95 s-pb-md-80 s-pt-xl-170 s-pb-xl-140 c-gutter-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-xl-4 animate" data-animation="scaleAppear">
                    <h2 class="mt-0 mb-40 contact-title text-uppercase">PUBLICATE EN</h2>
                    <span class="color-main fs-24 font-main text-uppercase">TUSENCUENTROS.CL</span>
                    
                 <!--   <div class="media mb-20">
                        <h5 class="fs-20 mb-0 min-w-100">Email:</h5>
                        <div class="media-body ml-0 d-flex flex-column">
                            <!--<span>contacto@tusencuentros.cl</span>
                        </div>
                    </div>-->
                </div>
                <!--.col-* -->
                <div class="fw-divider-space hidden-above-lg mt-20"></div>
                <div class="col-lg-7 col-xl-8 animate" data-animation="scaleAppear">                                
                    <form class="contact-form c-mb-20 c-gutter-20" method="POST" action="{{ route('register') }}">
                        @csrf
                        <input type="hidden" id="tipo_usuario" name="tipo_usuario" value="2">
    
                        @if($errors->any())
                            <div class="row">
                                <div class="col-12">
                                    <div class="alert alert-danger text-left">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="usuario" class="form-control" placeholder="Usuario">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="password" name="clave" class="form-control" placeholder="Clave">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select name="sexo" id="sexo" class="form-control" style="height: 60px;">
                                        <option value="1" selected>MUJER</option>
                                        <option value="2">HOMBRE</option>
                                    </select>
                                    <button type="submit" class="btn-submit"><i class="fa fa-paper-plane"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--.col-* -->
            </div>
        </div>
    </section>

    @if(count($anuncios) > 0)
    <section id="anuncios" class="ds s-pt-70 s-pb-20 s-pb-sm-50 s-py-lg-100 s-py-xl-150">
      <h5 class="big-title" style="font-size: 60px;line-height: initial;text-align:center;COLOR:#FFF">¿BUSCAS ALGO O DESEAS PUBLICAR?</h5>
      <h5 class="big-title" style="font-size: 50px;line-height: initial;text-align:center">AVISOS ER&OacuteTICOS</h5>
      
        <div class="container">
 
            @foreach ($anuncios as $item)
                <div class="row c-gutter-60">
                    <div class="offset-lg-1 col-lg-10">
                        <article class="post event side-item bordered">
                            <div class="row">
                                <div class="col-xl-3">
                                    <div class="item-media cover-image">
                                        <center>
                                            <img src="{{ asset('storage/'. $item->usuario->perfil_url) }}" alt="img" class="img-responsive" height="200px">
                                            <div class="media-links">
                                                <a class="abs-link" title="" href="{{ url('aviso/' . $item->id) }}"></a>
                                            </div>
                                        </center>
                                    </div>
                                </div>

                                <div class="col-xl-9">
                                    <div class="item-content">
                                        <div class="entry-content">
                                            <h3 class="entry-title">
                                                <a href="{{ url('aviso/' . $item->id) }}" class="name">{{ $item->titulo}}</a>
                                            </h3>
                                            <p> {{ substr($item->contenido, 0, 300) }} ...</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>

                    <div class="fw-divider-space hidden-below-lg mt-50"></div>
                </div>
            @endforeach

        </div>
    </section>
@endif
 
	<script src="https://kit.fontawesome.com/f755b555f3.js"></script>

@endsection
