@extends('layouts.modelo')

@section('contenido')

    <section class="page_breadcrumbs changeable ls gradient gorizontal_padding section_padding_20 columns_padding_5 table_section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 text-center text-sm-left darklinks">
                    <a href="#">
                        <em>{{ $usuario->telefono }}</em>
                    </a>
                </div>
                <div class="col-sm-6 text-center">
                    <ol class="center-block breadcrumb">
                        <li>
                            <a href="{{ url('/escorts') }}">
                                Inicio
                            </a>
                        </li>
                        <li class="active">
                            <span> Video de {{ $usuario->nombre}}</span>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-3 text-center text-sm-right">
                    <ul class="inline-dropdown inline-block">
                        <li class="dropdown login-dropdown">
                            @guest
								<li class="dropdown login-dropdown">
									<a class="topline-button" id="login" data-target="#" href="./" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
										<i class="rt-icon2-user"></i>
									</a>
									<div class="dropdown-menu ds" aria-labelledby="login">
                                        <form role="form" method="POST" action="{{ route('login') }}">
                                            @csrf
											<div class="form-group">
												<label for="email" class="sr-only">Email</label>
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
											</div>
											<div class="form-group">
												<label for="password" class="sr-only">Contraseña</label>
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
											</div>
											<button type="submit" class="theme_button color1 bottommargin_0">
												Ingresar
											</button>
											<div class="checkbox-inline">
												<input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label for="remember" class="bottommargin_0"> Recuérdame</label>
											</div>
										</form>
									</div>
                                </li>                                  
                            @else
                                <a class="topline-button" id="login" href="{{ url('/home')}}" role="button" >
                                    <i class="rt-icon2-user"></i> {{ $usuario->nombre }}
                                </a>
                            @endguest
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="ds ms">
        <div>
            <div class="container">
                <br>

                @if($usuario->video_url != null || $usuario->video_url != '')
                    <div class="row">
                        <div class="col-sm-10 col-sm-push-1">
                            <div class="embed-responsive embed-responsive-3by2">
                                <a href="{{ asset('storage/' . $usuario->video_url) }}" class="embed-placeholder">
                                    <img src="{{ asset('principal/images/gallery/02.jpg')}}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                @endif
                @foreach($videos AS $item)
                    <div class="row">
                        <div class="col-sm-10 col-sm-push-1">
                            <div class="embed-responsive embed-responsive-3by2">
                                <a href="{{ asset('storage/' . $item->video_url) }}" class="embed-placeholder">
                                    <img src="{{ asset('principal/images/gallery/02.jpg')}}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                @endforeach
            </div>
        </div>
    </section>

@endsection
