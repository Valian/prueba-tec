@extends('layouts.modelo')

@section('contenido')


    <header class="page_header ds bottom_mask_add"><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-xl-3 col-lg-4 col-md-5 col-11">
                      <a href="{{ redirect()->getUrlGenerator()->previous() }}" class="logo logo_image">
                        <img src="{{ asset("principal/images/logo.png") }}" alt="" style="max-height: 60px;">
                    </a>
                </div>
                <div class="col-xl-6 col-lg-8 col-md-7 col-1">
                    <div class="nav-wrap">

                        <!-- main nav start -->
                        <nav class="top-nav">
                            <ul class="nav sf-menu">
                                <li class="active"><a href="{{ redirect()->getUrlGenerator()->previous() }}">Inicio</a></li>
                                <!--<li><a href="#">Nosotros</a></li>-->
                                <li><a href="{{ redirect()->getUrlGenerator()->previous() }}/#escorts">Agencia</a></li>
                            </ul>
                        </nav>
                        <!-- eof main nav -->

                        <!--hidding includes on small devices. They are duplicated in topline-->

                    </div>
                </div>
               
            </div>
        </div>
        <!-- header toggler -->
        <span class="toggle_menu"><span></span></span>
    </header>
    
         <section id="publicateEscort" class="contacts-section top_mask_add ds overflow-visible background-contact s-pt-70 s-pb-60 s-pt-md-95 s-pb-md-80 s-pt-xl-170 s-pb-xl-140 c-gutter-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-xl-4 animate" data-animation="scaleAppear">
                    <h2 class="mt-0 mb-40 contact-title text-uppercase">PUBLICATE EN</h2>
                    <span class="color-main fs-24 font-main text-uppercase">TUSENCUENTROS.CL</span>
                    
                 <!--   <div class="media mb-20">
                        <h5 class="fs-20 mb-0 min-w-100">Email:</h5>
                        <div class="media-body ml-0 d-flex flex-column">
                            <!--<span>contacto@tusencuentros.cl</span>
                        </div>
                    </div>-->
                </div>
                <!--.col-* -->
                <div class="fw-divider-space hidden-above-lg mt-20"></div>
                <div class="col-lg-7 col-xl-8 animate" data-animation="scaleAppear">                                
                    <form class="contact-form c-mb-20 c-gutter-20" method="POST" action="{{ route('register') }}">
                        @csrf
                        <input type="hidden" id="tipo_usuario" name="tipo_usuario" value="2">
    
                        @if($errors->any())
                            <div class="row">
                                <div class="col-12">
                                    <div class="alert alert-danger text-left">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="usuario" class="form-control" placeholder="Usuario">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="password" name="clave" class="form-control" placeholder="Clave">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select name="sexo" id="sexo" class="form-control" style="height: 60px;">
                                        <option value="1" selected>MUJER</option>
                                        <option value="2">HOMBRE</option>
                                    </select>
                                    <button type="submit" class="btn-submit"><i class="fa fa-paper-plane"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--.col-* -->
            </div>
        </div>
    </section>

     @if(count($perfil_usuarios_2) > 0)
        <section id="perfil_usuarios_2" class="gallery-section gallery-6 bottom_mask_add overflow-visible ds s-pb-70 s-pb-md-80 s-pb-xl-155">
            <br><br><br>
            <h1 class="big-title" style="font-size:60px;text-align:center;line-height:initial"> PERFILES CON VIDEO</h1>
            <div class="container-fluid">
                <div class="row">
                    <div class="fw-divider-space hidden-below-xl pt-70"></div>
                    <div class="col-lg-12">
                       <!-- <div class="row isotope-wrapper masonry-layout c-gutter-30 c-mb-30 animate" data-animation="fadeInDown">-->
                       <div class="row  c-gutter-30 c-mb-30 " data-animation="fadeInDown">
                            @foreach ($perfil_usuarios_2 as $item)
                           @if($item->video_url!=NULL)
                                <div class="col-sm-4 col-lg-4 col-lgx-4 col-xl-4 col-xs-6 col-6">
                                    <div class="vertical-item item-gallery content-absolute text-center ds" style="padding-bottom: 30px;">
                                        <a href="{{ url('/modelo/' . $item->id . '/ver') }}" class="item-media h-100 w-100 d-block">
                                            <img src="{{ asset('storage/' . $item->perfil_url) }}" alt="Modelo">
                                            <div class="media-links"></div>
                                        </a>
                                             @if($item->telefono!=0)
                                    <a href="https://api.whatsapp.com/send?phone={{ $item->telefono }}" target="_blank" style="background-color: transparent;"><img src="https://clubvip.cl/storage/watsapp_logo.png" style="width:40px;height:40px;margin-top:5px"></a>
                                     <a href="tel:+{{ $item->telefono }}" target="_blank" style="background-color: transparent;"><i class="fa fa-phone-square" aria-hidden="true" style="font-size:36px;color:#fff;float:right"></i></a>
                                     @endif
                                        <div class="item-content" style="background-color: #e90ba5;">
                                            
                                            <div class="item-title" style="top:-65px;width:100%">
                                                <div style="display:inline-flex"> <h5>{{ $item->nombre }} </h5> 
                                                </div> 
                                                <div> <p style="font-size:14px">{{ $item->nacionalidad }}, {{ $item->edad }}</p> </div>
                                                <div> <p style="font-size:12px">Medidas: {{ $item->busto }}-{{ $item->cintura }}-{{ $item->caderas }}</p> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                  
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
 
	<script src="https://kit.fontawesome.com/f755b555f3.js"></script>

@endsection
