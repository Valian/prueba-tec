@extends('layouts.principal')

@section('contenido')

    <section class="ds section_padding_70 parallax">
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-2 col-sm-8 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8 text-center">
                    <h2 class="big margin_0">¿Qué deseas?</h2>
                    <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">Bienvenidos</h2>

                    <div class="row text-center">
                        <a href="#agencia" class="btn-block theme_button color1 bottommargin_5" style="margin-left: 6px; font-size: 30px;">ESCORT EN CONCEPCIÓN</a>
                        <a href="#avisos"  class="btn-block theme_button color1 bottommargin_5" style="margin-left: 0px; margin-bottom: 5px;" >AVISOS ERÓTICOS ESCORT</a>
                        <a href="{{ url('/videos') }}" class="btn-block theme_button color1 bottommargin_5" style="margin-left: 0px; margin-bottom: 5px;" >VIDEOS CALIENTES </a>
                        <a href="{{ url('/anuncios') }}" class="btn-block theme_button color1 bottommargin_5" style="margin-left: 0px; margin-bottom: 5px;" >GENTE BUSCA SEXO</a>
                        <button disabled type="button" class="btn-block theme_button color1 bottommargin_5" style="margin-left: 0px; margin-bottom: 5px;" >CHAT CALIENTE (Próximamente) </button>
                    </div>

                </div>
            </div>
        </div>
    </section>

    
    @include('welcome');
    @include('avisos');

    @if(count($noticias) > 0)

        <section class="ds section_padding_top_100 section_padding_bottom_50 columns_padding_25">
            <div class="container">
                <div class="row">
    
                    <div class="col-sm-10 col-sm-push-1">
    
                        
                        @foreach ($noticias as $item)
                            <article class="vertical-item post format-standard with_background">
                                    <a href="{{ url('noticia/' . $item->id) }}">
                                        <div class="item-media entry-thumbnail">
                                            <img src="{{ asset('storage/' . $item->img_url) }}" alt="" style="max-height: 350px; width: auto;">
                                        </div>
                
                                        <div class="item-content entry-content">
                                            <header class="entry-header">
                
                                                <h4 class="entry-title">
                                                    <a href="{{ url('noticia/' . $item->id) }}" rel="bookmark text-center">{{ $item->titulo }}</a>
                                                </h4>
                
                
                                            </header>
                                            <!-- .entry-header -->
                
                                            <p class="text-justify">
                                                {!! substr($item->contenido, 0, 200) !!} ...    
                                            </p>
                
                                        </div>
                                        <!-- .item-content.entry-content -->
                                    </a>
                            </article>
                        @endforeach

                        <!-- .post -->
    
                    </div>
                    <!--eof .col-sm-8 (main content)-->
    
    
                </div>
            </div>
        </section>


    @endif

    <section id="nosotros" class="ds intro_section">
        <div class="flexslider vertical-nav">
            <ul class="slides">
                <li>
                    <img src="{{ asset('principal/images/slide01.png') }}" alt="">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center text-md-right">
                                <div class="slide_description_wrapper">
                                    <div class="slide_description">
                                        <div class="intro-layer" data-animation="slideExpandUp">
                                            <h2 class="big margin_0">Sobre Placer Conce</h2>
                                            <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">Algo que debes saber</h2>
                                        </div>
                                        <div class="intro-layer" data-animation="slideExpandUp">
                                            <p class="bottommargin_30">
                                                Este portal te invita a dar rienda suelta al deseo y el placer que llevas dentro de ti, a través de la sección "<a href="{{ url('/escorts') }}">ESCORT EN CONCEPCIÓN</a>" contactaras con las mejores escort, scort, prostitutas, putas, damas de compañia o como quieras llamarlas, todas ellas ubicadas cerca de ti, en Concepción o la octava región, puedes ver los número de ellas y contactarlas directamente para preguntar sobre sus servicios sexuales, queremos dar un espacio al sexo en Concepción, construir un plataforma para la interacción de personas que buscan disfrutar del placer sexual, sin embargo consideramos que esta interacción debe darse en un contexto de respeto, por lo cual cualquiera que no acepte estas reglas será eliminado permanentemente del portal.                                             
                                            </p>
                                            <p>
                                                Con la categorización, podrás encontrar tus escort mediante sus cualidades, ejemplo: escort con trayectoria destacada, escort jovenes, escort maduras, escort rubias, escort morenas, escort voluptuosas, escort delgadas, escort debutantes, escort con senos grandes, escort culonas, despedidas de solteros, trios, fantasías, fetiches,etc 
                                           </p>
                                        </div>
                                    </div>
                                    <!-- eof .slide_description -->
                                </div>
                                <!-- eof .slide_description_wrapper -->
                            </div>
                            <!-- eof .col-* -->
                        </div>
                        <!-- eof .row -->
                    </div>
                    <!-- eof .container -->
                </li>
                <li>
                    <img src="{{ asset('principal/images/slide01.png') }}" alt="">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center text-md-right">
                                <div class="slide_description_wrapper">
                                    <div class="slide_description">
                                        <div class="intro-layer" data-animation="slideExpandUp">
                                            <h2 class="big margin_0">Sobre Placer Conce</h2>
                                            <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">Algo que debes saber</h2>
                                        </div>
                                        <div class="intro-layer" data-animation="slideExpandUp">
                                            <p class="bottommargin_30">
                                                En el área "<a href="{{ url('/publicaciones') }}">AVISOS EROTICOS</a>" podras encontrar avisos en tiempo real de las escort, lo que te indica cuales son las que están disponibles ahora ya, y hace cuanto tiempo publicaron, y si necesitas más información de la chica, puedes a través de su aviso, llegar directamente a su perfil.
                                            </p>
                                            <p>
                                                A través del Área "<a href="{{ url('/anuncios') }}">GENTE QUE BUSCA SEXO</a>" podras encontrar gente en tu ciudad que busca pasar un buen rato sin compromiso y disfrutar del placer sexual, a través de sus perfiles podrás obtener más información de ellos y poder concretar un encuentro.
                                           </p>
                                            <p>
                                                En la sección “CHAT CALIENTE” podrás contactarte con gente que le encanta el sexo, y compartir entretenidos temas y a la vez opiniones sobre como pasar un buen rato en concepción, los mejores lugares, datos, picadas, novedades, etc. y compartir un rato de camaradería en tiempo real.
                                            </p>
                                        </div>
                                    </div>
                                    <!-- eof .slide_description -->
                                </div>
                                <!-- eof .slide_description_wrapper -->
                            </div>
                            <!-- eof .col-* -->
                        </div>
                        <!-- eof .row -->
                    </div>
                    <!-- eof .container -->
                </li>
                <li>
                        <img src="{{ asset('principal/images/slide01.png') }}" alt="">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12 text-center text-md-right">
                                    <div class="slide_description_wrapper">
                                        <div class="slide_description">
                                            <div class="intro-layer" data-animation="slideExpandUp">
                                                <h2 class="big margin_0">Sobre Placer Conce</h2>
                                                <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">Algo que debes saber</h2>
                                            </div>
                                            <div class="intro-layer" data-animation="slideExpandUp">
                                                <p class="bottommargin_30">
                                                    Finalmente en la sección “VIDEOS CALIENTES” te invitamos a compartir y subir material multimedia, para que la pases bien, disfrutando de ver sexo.
                                                </p>
                                                <p>
                                                    En fin te invitamos a que disfrutes de una agradable experiencia en tu ciudad y conectarte con personas que están ofreciendo o buscando una experiencia sexual al igual que tu.
                                                </p>
                                            </div>
                                        </div>
                                        <!-- eof .slide_description -->
                                    </div>
                                    <!-- eof .slide_description_wrapper -->
                                </div>
                                <!-- eof .col-* -->
                            </div>
                            <!-- eof .row -->
                        </div>
                        <!-- eof .container -->
                    </li>
            </ul>
        </div>
        <!-- eof flexslider -->
    </section>


	<script src="https://kit.fontawesome.com/f755b555f3.js"></script>

@endsection
